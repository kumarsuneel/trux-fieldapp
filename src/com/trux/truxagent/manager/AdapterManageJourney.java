package com.trux.truxagent.manager;

import java.util.ArrayList;

import com.squareup.picasso.Picasso;
import com.trux.app.truxfield.R;
import com.trux.app.truxfield.model.Driver;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

public class AdapterManageJourney extends ArrayAdapter<Driver> {
	Context context;

	public AdapterManageJourney(Context context, int textViewResourceId, ArrayList<Driver> objects) {
		super(context, textViewResourceId, objects);
		this.context = context;

	}

	public View getView(int position, View convertView, ViewGroup parent) {
		if (convertView == null) {
			LayoutInflater inflater = (LayoutInflater) getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
			convertView = inflater.inflate(R.layout.adapter_manage_journey, null);
		}
		ImageView driverIV = (ImageView) convertView.findViewById(R.id.driverIV);
		TextView driverNameTV = (TextView) convertView.findViewById(R.id.driverNameTV);
		TextView driverPhoneTV = (TextView) convertView.findViewById(R.id.driverPhoneTV);
		TextView vehicleNumberTV = (TextView) convertView.findViewById(R.id.vehicleNumberTV);
		Driver driver = getItem(position);
		driverNameTV.setText(driver.driverName);
		driverPhoneTV.setText(driver.driverPhoneNumber);
		vehicleNumberTV.setText(driver.vehicleNumber);
		 Picasso.with(context) //
		 .load(driver.driverImage) //
		 .placeholder(R.drawable.ic_launcher) //
//		 .error(R.drawable.logo2) //
//		 .fit() //
		 .into(driverIV);
		return convertView;
	}

}
