package com.trux.truxagent.utils;

import java.util.Locale;
import java.util.StringTokenizer;

import com.trux.app.truxfield.model.SharedPreferencesField;

import android.content.Context;
import android.content.res.Configuration;
import android.os.StrictMode;
/**
 * Created by anurag.srivastava.
 */
public class CommonUtils  {
	
	private int SDK_INT;
	private static Locale myLocale;
	private static Context mContext;
    public CommonUtils(){

    }
	public void checkStrictMode() {
		SDK_INT = android.os.Build.VERSION.SDK_INT;
		if (SDK_INT>8){
			StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
   		 	StrictMode.setThreadPolicy(policy);
		}
	}
	public static Locale getLocale(String language) {
		StringTokenizer mStringTokenizer = new StringTokenizer(language, " ");
		language = mStringTokenizer.nextToken();
		Locale local = Locale.ENGLISH;
		if (language.contains("Hindi"))
			local = new Locale("hi");
		else if (language.contains("Bengali"))
			local = new Locale("bn");
		else if (language.contains("Kannada"))
			local = new Locale("kn");
		else if (language.contains("Malayalam"))
			local = new Locale("ml");
		else if (language.contains("Marathi"))
			local = new Locale("mr");
		else if (language.contains("Punjabi"))
			local = new Locale("pa");
		else if (language.contains("Tamil"))
			local = new Locale("ta");
		else if (language.contains("Telugu"))
			local = new Locale("te");
		return local;
	}
	public static void setLocale(String lang, Context context, SharedPreferencesField mSPT) {
		mSPT.setString("SELECT_LANGUAGE", lang);
		mContext = context;
		myLocale = new Locale(lang);
		Locale.setDefault(myLocale);
		Configuration config = new Configuration();
		config = mContext.getResources().getConfiguration();
		config.locale = myLocale;
		mContext.getResources().updateConfiguration(config,mContext.getResources().getDisplayMetrics());
	}
}
