package com.trux.truxagent.utils;

import java.io.InputStream;
import java.io.OutputStream;
import java.io.UnsupportedEncodingException;

import com.trux.app.truxfield.MainControllerApplication;

import android.content.Context;
import android.util.TypedValue;

public class Utils {

	private static String mAuthToken;
	
	private static int userId;
	public static final boolean isFileCache = true;
	public static final boolean isMemCache = true;
	public static boolean IMAGE_MEMORY_CACHE=false;
	public static boolean IMAGE_FILE_CACHE=true;
	
	
	/**
	 * method to get the auth token received after successful login
	 * @param context -> the application context
	 * @return -> auth token
	 */
	public static String getmAuthToken(Context context) {
		if(mAuthToken == null || mAuthToken.equals("")){
			mAuthToken = PreferenceManager.getStringPreference(context, "authToken");
		}
		return mAuthToken;
	}

	/**
	 * Method to set the auth token retrieved after successful call to login api
	 * @param context	->	 the application context
	 * @param authToken	-> the token received.
	 */
	public static void setmAuthToken(Context context, String authToken) {
		Utils.mAuthToken = authToken;
		PreferenceManager.setStringPreference(context, "authToken", authToken);
	}
	
	

	public static int getUserId() {
		return userId;
	}

	public static void setUserId(int userId) {
		Utils.userId = userId;
	}
	
	/**
	 * Method to replace special characters with url encoded characters
	 * 
	 * @param str -> String to encode
	 * @return -> encoded string
	 * @throws UnsupportedEncodingException 
	 */
	public static String convertURL(String str){
	    String url = null;
	    try{
	    url = new String(str.trim().replace("%", "%25").replace(" ", "%20").replace("&", "%26")
	            .replace(",", "%2c").replace("(", "%28").replace(")", "%29")
	            .replace("!", "%21").replace("=", "%3D").replace("<", "%3C")
	            .replace(">", "%3E").replace("#", "%23").replace("$", "%24")
	            .replace("'", "%27").replace("*", "%2A").replace("-", "%2D")
	            .replace(".", "%2E").replace("/", "%2F").replace(":", "%3A")
	            .replace(";", "%3B").replace("?", "%3F").replace("@", "%40")
	            .replace("[", "%5B").replace("\\", "%5C").replace("]", "%5D")
	            .replace("_", "%5F").replace("`", "%60").replace("{", "%7B")
	            .replace("|", "%7C").replace("}", "%7D"));
	    }catch(Exception e){
	        e.printStackTrace();
	    }
		return url;
	}

    /**
     * Method to return converted PX value of given DP
     * @param dp    -> value to convert
     * @return      -> converted value
     */
    public static int convertDpToPx(int dp){
        float px = TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, dp, MainControllerApplication.getInstance().getApplicationContext().getResources().getDisplayMetrics());
        return (int) px;
    }
    
    public static void CopyStream(InputStream is, OutputStream os)
    {
        final int buffer_size=1024;
        try
        {
            byte[] bytes=new byte[buffer_size];
            for(;;)
            {
              int count=is.read(bytes, 0, buffer_size);
              if(count==-1)
                  break;
              os.write(bytes, 0, count);
            }
        }
        catch(Exception ex){}
    }
    
  
}
