package com.trux.app.truxfield.network;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.StrictMode;
import android.telephony.TelephonyManager;
/**
 * Created by anurag.srivastava.
 */
public class NetworkStateReceiver {
	
	private TelephonyManager mTelephonyManager;
	private boolean isEnabled;
	private int SDK_INT;
	
	public static boolean isOnline(Context context) {
		
		ConnectivityManager connectivity = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
		if (connectivity != null) {
			NetworkInfo[] info = connectivity.getAllNetworkInfo();
			if (info != null) {
				for (int i = 0; i < info.length; i++) {
					if (info[i].getState() == NetworkInfo.State.CONNECTED) {
						return true;
					}
				}
			}
		}
		return false;
	}
	public boolean isOnlinetrue(Context context) {
		
		SDK_INT = android.os.Build.VERSION.SDK_INT;
		if (SDK_INT>8){
  		 StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
  		 StrictMode.setThreadPolicy(policy); 
		}
		mTelephonyManager = (TelephonyManager) context.getSystemService(Context.TELEPHONY_SERVICE);
		if(mTelephonyManager.getDataState() == TelephonyManager.DATA_CONNECTED){
		    isEnabled  = true;
		}else{
		    isEnabled = false;  
		}
		return isEnabled;   
	}
}
