package com.trux.app.truxfield;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.v7.app.AlertDialog;
import android.view.KeyEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.Toast;

import com.trux.app.truxfield.model.SharedPreferencesField;
import com.trux.app.truxfield.network.NetworkStateReceiver;
import com.trux.app.truxfield.services.UrlService;

public class CRMActivity extends Activity implements OnClickListener{
	
	private WebView webView;
	private Button btnRetry;
	private Button btnHome;
	private SharedPreferencesField mSPLF;
	private LinearLayout mlLayoutRequestError = null;
	private Handler mhErrorLayoutHide = null;
	private boolean mbErrorOccured = false;
	private boolean mbReloadPressed = false;
	private Intent mIntent;
	
	@SuppressWarnings("deprecation")
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_crm);
		
		mSPLF = new SharedPreferencesField(getApplicationContext());
		mlLayoutRequestError = (LinearLayout) findViewById(R.id.lLayoutRequestError);
        mhErrorLayoutHide = getErrorLayoutHideHandler();
        btnHome = (Button) findViewById(R.id.btnHome);
        btnRetry = (Button) findViewById(R.id.btnRetry);
        btnRetry.setOnClickListener(this);
		webView=(WebView)findViewById(R.id.webView);
		webView.getSettings().setJavaScriptEnabled(true);
		webView.getSettings().setLoadWithOverviewMode(true);
		webView.getSettings().setUseWideViewPort(true);
		webView.getSettings().setLoadsImagesAutomatically(true);
		webView.getSettings().setLightTouchEnabled(true);
		webView.getSettings().setDomStorageEnabled(true);
		webView.setScrollBarStyle(WebView.SCROLLBARS_OUTSIDE_OVERLAY);
		webView.addJavascriptInterface(new MyJavaScriptInterface(this), "HtmlViewer");
		webView.setScrollbarFadingEnabled(false);
		webView.setVerticalScrollBarEnabled(false);
		webView.setHorizontalScrollBarEnabled(false);
		webView.setScrollContainer(false);
		webView.clearHistory();
		webView.clearCache(true);
		webView.setInitialScale(1);
		startWebView(UrlService.BASE_URL+UrlService.LOGIN_CRM_AUTHENTICATION+mSPLF.getString("authentication", ""));
		
		
		btnHome.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
            	mIntent = new Intent(CRMActivity.this, MainActivity.class)
    	            	.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP).addFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP);
    	            	startActivity(mIntent);
    	                finish();
            }
        });
	}
	private void startWebView(String url) {
        //When opening a url or click on link
        webView.setWebViewClient(new WebViewClient() {     
            //If you will not use this method url links are opeen in new brower not in webview
            public boolean shouldOverrideUrlLoading(WebView view, String url) {             
                view.loadUrl(url);
                return true;
            }
            @Override
            public void onPageStarted(WebView view, String url, Bitmap favicon) {
                super.onPageStarted(view, url, favicon);
            }

            @Override
            public void onLoadResource(WebView view, String url) {
                super.onLoadResource(view, url);
            }

            @Override
            public void onPageFinished(WebView view, String url) {
                if (mbErrorOccured == false && mbReloadPressed) {
                    hideErrorLayout();
                    mbReloadPressed = false;
                }

                super.onPageFinished(view, url);
            }
            public void onReceivedError(WebView view, int errorCode, String description, String failingUrl) {
//                Toast.makeText(CRMActivity.this, "Oh no! " + description, Toast.LENGTH_SHORT).show();
//                showErrorLayout();
                if(NetworkStateReceiver.isOnline(getApplicationContext())){
                	
                }else{
                	view.loadUrl("about:blank");
                	Toast.makeText(CRMActivity.this, getResources().getString(R.string.error_internet), Toast.LENGTH_LONG).show();
                }
                
              }
        });
        webView.loadUrl(url);
    }
	@Override
	public boolean onKeyDown(int keyCode, KeyEvent event) {
	    if(event.getAction() == KeyEvent.ACTION_DOWN){
	        switch(keyCode){
	        case KeyEvent.KEYCODE_BACK:
	            if(webView.canGoBack() == true){
	            	webView.goBack();
	            }else{
	            	mIntent = new Intent(CRMActivity.this, MainActivity.class)
	            	.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP).addFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP);
	            	startActivity(mIntent);
	                finish();
	            }
	            return true;
	        }
	    }
	    return super.onKeyDown(keyCode, event);
	   }
	class MyJavaScriptInterface {

        private Context ctx;

        MyJavaScriptInterface(Context ctx) {
            this.ctx = ctx;
        }

        public void showHTML(String html) {
            new AlertDialog.Builder(ctx).setTitle("HTML").setMessage(html)
                    .setPositiveButton(android.R.string.ok, null).setCancelable(false).create().show();
        }

    }

//	private void showErrorLayout() {
//		webView.setVisibility(View.GONE);
//        mlLayoutRequestError.setVisibility(View.VISIBLE);
//        
//    }

    private void hideErrorLayout() {
        mhErrorLayoutHide.sendEmptyMessageDelayed(10000, 200);
    }

    private Handler getErrorLayoutHideHandler() {
        return new Handler() {
            @Override
            public void handleMessage(Message msg) {
                mlLayoutRequestError.setVisibility(View.GONE);
                super.handleMessage(msg);
            }
        };
    }
	@Override
	public void onClick(View v) {
		if (!mbErrorOccured) {
            return;
        }
        mbReloadPressed = true;
        webView.reload();
        mbErrorOccured = false;
	}
}
