package com.trux.app.truxfield.interfaces;

public interface OnServiceResponseListener {
	public void onServiceResponse(StringBuilder result, int responseCode);
	public void onServiceError();
}
