package com.trux.app.truxfield;

import java.text.SimpleDateFormat;
import java.util.Calendar;

import org.json.JSONObject;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.trux.app.truxfield.interfaces.OnServiceResponseListener;
import com.trux.app.truxfield.model.ResponseCode;
import com.trux.app.truxfield.model.SharedPreferencesField;
import com.trux.app.truxfield.network.NetworkStateReceiver;
import com.trux.app.truxfield.services.AsyncTaskService;
import com.trux.app.truxfield.services.AsyncTaskService.MODE;
import com.trux.app.truxfield.services.UrlService;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.KeyEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemSelectedListener;
import android.widget.Button;
import android.widget.Toast;

public class TripManagement extends Activity implements OnServiceResponseListener, OnItemSelectedListener {
	private Button btnScan;
	private Button btnMngJry;
	private Intent mIntent;
	private StringBuilder mStringBuilder;
	private SharedPreferencesField mSPLF;
	private Bundle msavedInstanceState;
	private MainControllerApplication mMainControllerApplication;

	@Override
	public boolean onKeyDown(int keyCode, KeyEvent event) {
		if ((keyCode == KeyEvent.KEYCODE_BACK)) {
			mIntent = new Intent(TripManagement.this, MainActivity.class).addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP)
					.addFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP);
			startActivity(mIntent);
			finish();
		}
		return super.onKeyDown(keyCode, event);
	}

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		msavedInstanceState = savedInstanceState;
		mSPLF = new SharedPreferencesField(getApplicationContext());
		mMainControllerApplication = (MainControllerApplication) getApplicationContext();
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_tripmanagement);
		mSPLF.setInt("ID", 0);
		btnScan = (Button) findViewById(R.id.btnScan);
		btnMngJry = (Button) findViewById(R.id.btnMngJry);

		btnScan.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				switchToScaningActivity();
			}
		});

		btnMngJry.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				getJsonData();

			}
		});
	}

	private void getJsonData() {
		final RequestQueue queue = Volley.newRequestQueue(this);
		mSPLF = new SharedPreferencesField(getApplicationContext());
		mStringBuilder = new StringBuilder();
		Calendar c = Calendar.getInstance();
		System.out.println("Current time => " + c.getTime());

		SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd");
		String formattedDate = df.format(c.getTime());
		mStringBuilder.append("userId=" + mSPLF.getString("id", null)).append("&currentDate=" + formattedDate);

		mStringBuilder.toString();

		String url = UrlService.BASE_URL + UrlService.MANAGE_JOURNEY + mStringBuilder;
		LoginActivity.showLog("" + url);
		JsonObjectRequest jsObjRequest = new JsonObjectRequest(Request.Method.GET, url, null,
				new Response.Listener<JSONObject>() {

					@Override
					public void onResponse(JSONObject resultJson) {

						LoginActivity.showLog("" + resultJson);
						try {

							if (resultJson.getInt("errorCode") == 101) {
								switchToScaningActivity();
							} else {
								switchToManageJourneyActivity(resultJson);

							}

						} catch (Exception e) {

							LoginActivity.showLog("" + e);

							LoginActivity.showToast(TripManagement.this, "" + e);
						}
					}

				}, new Response.ErrorListener() {

					@Override
					public void onErrorResponse(VolleyError error) {

						LoginActivity.showLog("" + error);

						LoginActivity.showToast(TripManagement.this, "" + error);
					}
				});

		queue.add(jsObjRequest);

	}

	private void switchToScaningActivity() {
		try {
			Intent intent = new Intent("com.google.zxing.client.android.SCAN");
			intent.putExtra("SCAN_MODE", "QR_CODE_MODE,PRODUCT_MODE");
			startActivityForResult(intent, 0);
		} catch (Exception e) {
			e.printStackTrace();
			Toast.makeText(getApplicationContext(), "ERROR:" + e, Toast.LENGTH_SHORT).show();
		}

	}

	private void switchToManageJourneyActivity(JSONObject resultJson) {
		Intent intent = new Intent(this, ManageJourneyActivity.class);
		intent.putExtra("journey_list", "" + resultJson);
		startActivity(intent);

	}

	public void onActivityResult(int requestCode, int resultCode, Intent intent) {
		if (requestCode == 0) {
			if (resultCode == RESULT_OK) {

				if (NetworkStateReceiver.isOnline(getApplicationContext())) {
					try {
						String number = intent.getStringExtra("SCAN_RESULT");
						if (number.contains("-")) {
							String[] parts = number.split("-");
							number = parts[1];
						}
						mStringBuilder = new StringBuilder();
						mStringBuilder.append("driver").append("/").append(number);
						new AsyncTaskService(this, UrlService.SEARCH_DRIVER, ResponseCode.SEARCHDRIVER, MODE.POST,
								mStringBuilder.toString(), true).execute();

					} catch (Exception e) {
						e.printStackTrace();
					}
				} else {

					mMainControllerApplication.showError(getResources().getString(R.string.error_internet),
							R.drawable.repeating_bg_error);
				}
			} else if (resultCode == RESULT_CANCELED) {
				mMainControllerApplication.showError(getResources().getString(R.string.error_scan),
						R.drawable.repeating_bg_error);
			}
		}
	}

	@Override
	public void onServiceResponse(StringBuilder result, int responseCode) {
		try {
			if (responseCode == ResponseCode.SEARCHDRIVER) {

				JSONObject resultJson = new JSONObject(result.toString());
				if (resultJson.getInt("errorCode") == 101) {
					Toast.makeText(TripManagement.this, "No data found for given number", Toast.LENGTH_LONG).show();
				} else {
					mIntent = new Intent(getApplicationContext(), DriverTripDetailActivity.class);
					mIntent.putExtra("Json", resultJson.toString());
					mIntent.putExtra("from_activity", 0);
					startActivity(mIntent);
					finish();
				}

			} else {
				((MainControllerApplication) getApplication())
						.showError(getResources().getString(R.string.error_no_data), R.drawable.repeating_bg_error);
			}
		} catch (Exception e) {
			((MainControllerApplication) getApplication()).showError(getResources().getString(R.string.error_service),
					R.drawable.repeating_bg_error);
		}
	}

	@Override
	public void onServiceError() {
		mMainControllerApplication.showError(getResources().getString(R.string.error_service),
				R.drawable.repeating_bg_error);
	}

	@Override
	public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
		if (mSPLF.getInt("ID", 0) == 1) {
			onRefresh();
		} else {
			mSPLF.setInt("ID", 1);
		}
	}

	@Override
	public void onNothingSelected(AdapterView<?> parent) {
		// TODO Auto-generated method stub

	}

	private void onRefresh() {
		mSPLF.setInt("ID", 0);
		onCreate(msavedInstanceState);
	}
}