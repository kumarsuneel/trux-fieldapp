package com.trux.app.truxfield.services;

import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.lang.ref.WeakReference;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.List;

import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.client.methods.HttpPut;
import org.apache.http.client.methods.HttpRequestBase;
import org.apache.http.entity.StringEntity;
import org.apache.http.entity.mime.HttpMultipartMode;
import org.apache.http.entity.mime.MultipartEntity;
import org.apache.http.entity.mime.content.ByteArrayBody;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.params.BasicHttpParams;
import org.apache.http.params.HttpConnectionParams;
import org.apache.http.params.HttpParams;
import org.apache.http.protocol.HTTP;
import org.apache.http.protocol.HttpContext;
import org.json.JSONObject;

import com.trux.app.truxfield.MainControllerApplication;
import com.trux.app.truxfield.R;
import com.trux.app.truxfield.interfaces.OnServiceResponseListener;
import com.trux.app.truxfield.model.ResponseCode;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.os.AsyncTask;
import android.os.Looper;
import android.os.StrictMode;

@SuppressWarnings("deprecation")
public class AsyncTaskService extends AsyncTask<JSONObject, Void, Void> {
	private ProgressDialog progressDialog;
	// private Dialog progressDialog;
	private WeakReference<Activity> mWeakreReference;
	private String url;
	private int what;
	private MODE mMode;
	private StringBuilder resultObj;
	Context mContext;
	private String PATH;
	boolean isProgressDialogRequired;
	private DefaultHttpClient httpClient;
	private HttpRequestBase request;
	private StringBuilder mStringBuilder;
	private static BufferedReader mBufferedReader;
	InputStream mInputStream;
	private List<NameValuePair> nameValuePairs;
	private List<NameValuePair> headers;

	public static enum MODE {
		POST, GET, PUT
	}

	public AsyncTaskService(Activity context, String url, int responseCode, MODE mode, String string,
			boolean isProgressDialogRequired) {
		this.mContext = context;
		mWeakreReference = new WeakReference<Activity>(context);
		this.what = responseCode;
		this.mMode = mode;
		this.PATH = string;
		this.url = url;
		this.isProgressDialogRequired = isProgressDialogRequired;
	}

	@Override
	protected void onCancelled() {
		super.onCancelled();
	}

	@Override
	protected void onPreExecute() {
		super.onPreExecute();
		if (isProgressDialogRequired) {
			progressDialog = ProgressDialog.show(mWeakreReference.get(), "",
					mWeakreReference.get().getResources().getString(R.string.loading), true);
		}
	}

	@Override
	protected void onPostExecute(Void result) {
		super.onPostExecute(result);

		if (progressDialog != null)
			progressDialog.dismiss();
		progressDialog = null;

		try {
			if (resultObj.toString().equals(""))
				return;
			if (mWeakreReference.get() != null)
				((OnServiceResponseListener) mWeakreReference.get()).onServiceResponse(resultObj, what);
		} catch (Exception e) {
			System.out.println(e.toString());
			if (mWeakreReference.get() != null)
				((OnServiceResponseListener) mWeakreReference.get()).onServiceError();
		}
	}

	@Override
	protected Void doInBackground(JSONObject... params) {
		try {
			final Activity context = mWeakreReference.get();
			if (context != null) {
				final MainControllerApplication app = ((MainControllerApplication) ((Activity) context)
						.getApplication());
				if (!app.hasConnection()) {
					return null;
				}
				if (Looper.getMainLooper() == null)
					Looper.prepare();
				if (params.length == 0)
					resultObj = executeAsync(null, url, mMode);
				else
					resultObj = executeAsync(params[0], url, mMode);
			}
		} catch (Exception e) {
			System.out.println("Async task " + url + "  " + e.toString());
		}
		return null;
	}

	private InputStream executeURL(JSONObject jsonObj, String url1, MODE mode) {

		InputStream inputStream = null;
		// String response = null;
		HttpURLConnection urlConnection = null;

		// Integer result = 0;
		try {

			URL url = new URL(url1);

			urlConnection = (HttpURLConnection) url.openConnection();

			urlConnection.setRequestProperty("Content-Type", "application/json");

			/* optional request header */
			urlConnection.setRequestProperty("Accept", "application/json");

			/* for Get request */
			if (mode == MODE.POST)
				urlConnection.setRequestMethod("POST");
			else if (mode == MODE.PUT)
				urlConnection.setRequestMethod("PUT");
			else
				urlConnection.setRequestMethod("GET");

			inputStream = new BufferedInputStream(urlConnection.getInputStream());

		} catch (Exception e) {
			e.printStackTrace();
		}

		return inputStream; // "Failed to fetch data!";

	}

	private StringBuilder executeAsync(JSONObject jsonObj, String url, MODE mode) {

		// httpClient = new DefaultHttpClient();

		try {
			if (what == ResponseCode.UPLOADDOCUMENT) {
				if (mode == MODE.POST)
					request = new HttpPost(UrlService.BASE_URL + url);
				else if (mode == MODE.PUT)
					request = new HttpPut(UrlService.BASE_URL + url);
				else
					request = new HttpGet(UrlService.BASE_URL + url);

				StringEntity entity = new StringEntity(PATH, HTTP.UTF_8);
				entity.setContentType("application/json");
				if (mode == MODE.POST)
					((HttpPost) request).setEntity(entity);
				else if (mode == MODE.PUT)
					((HttpPut) request).setEntity(entity);

				int timeoutConnection = 60000;
				HttpParams httpParameters = new BasicHttpParams();
				HttpConnectionParams.setConnectionTimeout(httpParameters, timeoutConnection);
				int timeoutSocket = 60000;
				HttpConnectionParams.setSoTimeout(httpParameters, timeoutSocket);
				HttpConnectionParams.setTcpNoDelay(httpParameters, true);

				mInputStream = executeURL(jsonObj, UrlService.BASE_URL + url, mode);

				// HttpResponse response = httpClient.execute(request);
				// mInputStream = response.getEntity().getContent();
				mBufferedReader = new BufferedReader(new InputStreamReader(mInputStream));
				String line = null;
				mStringBuilder = new StringBuilder();
				while ((line = mBufferedReader.readLine()) != null) {
					mStringBuilder.append(line);
				}
				longMsg(mStringBuilder.toString());
				// response.getEntity().consumeContent();
				mInputStream.close();
			}

			if (what == ResponseCode.UPLOADIMAGE) {
				// //CloseableHttpClient httpClient=
				// HttpClients.createDefault();
				// OkHttpClient client = new OkHttpClient();
				StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
				StrictMode.setThreadPolicy(policy);
				byte[] data = PATH.getBytes();// bos.toByteArray();

				HttpPost postRequest = new HttpPost("http://staging.truxapp.com/trux/lgapi/uploadImage");
				String fileName = String.format("File_%d.png", new Date().getTime());
				ByteArrayBody bab = new ByteArrayBody(data, fileName);
				// ContentBody mimePart = bab;
				MultipartEntity reqEntity = new MultipartEntity(HttpMultipartMode.BROWSER_COMPATIBLE);
				reqEntity.addPart("uploadfiles", bab);
				postRequest.setEntity(reqEntity);
				int timeoutConnection = 60000;
				HttpParams httpParameters = new BasicHttpParams();
				HttpConnectionParams.setConnectionTimeout(httpParameters, timeoutConnection);
				int timeoutSocket = 60000;
				HttpConnectionParams.setSoTimeout(httpParameters, timeoutSocket);
				HttpConnectionParams.setTcpNoDelay(httpParameters, true);
				// mInputStream=uploadFile(data,
				// "http://staging.truxapp.com/trux/lgapi/uploadImage");
				/// HttpResponse response = httpClient.execute(postRequest);
				httpClient.execute(postRequest);
				// mBufferedReader = new BufferedReader(new
				// InputStreamReader(response.getEntity().getContent(),
				// "UTF-8"));
				mBufferedReader = new BufferedReader(new InputStreamReader(mInputStream));
				String sResponse;
				mStringBuilder = new StringBuilder();
				while ((sResponse = mBufferedReader.readLine()) != null) {
					mStringBuilder.append(sResponse);
				}
				JSONObject jo = new JSONObject();
				jo.put("ImagePath", mStringBuilder.toString());
				jo.put("errorCode", 100);
				jo.put("errorMesaage", "successesful");
				mStringBuilder = new StringBuilder();
				mStringBuilder.append(jo);
			}
			if (what == ResponseCode.PUNCHIN || what == ResponseCode.STARATJOURNEY || what == ResponseCode.STOPJOURNEY
					|| what == ResponseCode.SEARCHDRIVER || what == ResponseCode.PUNCHOUT
					|| what == ResponseCode.UPLOADDOCUMENT || what == ResponseCode.LOGIN) {
				if (mode == MODE.POST)
					request = new HttpPost(UrlService.BASE_URL + url + PATH);
				else if (mode == MODE.PUT)
					request = new HttpPut(UrlService.BASE_URL + url + PATH);
				else
					request = new HttpGet(UrlService.BASE_URL + url + PATH);
				int timeoutConnection = 60000;
				HttpParams httpParameters = new BasicHttpParams();
				HttpConnectionParams.setConnectionTimeout(httpParameters, timeoutConnection);
				int timeoutSocket = 60000;
				HttpConnectionParams.setSoTimeout(httpParameters, timeoutSocket);
				HttpConnectionParams.setTcpNoDelay(httpParameters, true);
				// HttpResponse response = httpClient.execute(request);
				// mInputStream = response.getEntity().getContent();

				mInputStream = executeURL(jsonObj, UrlService.BASE_URL + url + PATH, mode);

				mBufferedReader = new BufferedReader(new InputStreamReader(mInputStream));
				String line = null;
				mStringBuilder = new StringBuilder();
				while ((line = mBufferedReader.readLine()) != null) {
					mStringBuilder.append(line);
				}
				longMsg(mStringBuilder.toString());
				// response.getEntity().consumeContent();
				mInputStream.close();
			} else {
				if (jsonObj != null) {
					nameValuePairs = new ArrayList<NameValuePair>();
					headers = new ArrayList<NameValuePair>();
					Iterator<String> iter = jsonObj.keys();
					while (iter.hasNext()) {
						String key = iter.next();
						try {
							String value = jsonObj.get(key).toString();
							nameValuePairs.add(new BasicNameValuePair(key, value));
						} catch (Exception e) {
							e.printStackTrace();
						}
					}
				}
				if (mode == MODE.GET) {
					PATH = "";
					if (!nameValuePairs.isEmpty()) {
						// PATH += "?";
						for (NameValuePair p : nameValuePairs) {
							String paramString = p.getName() + "=" + URLEncoder.encode(p.getValue(), "UTF-8");
							if (PATH.length() > 1) {
								PATH += "&" + paramString;
							} else {
								PATH += paramString;
							}
						}
					}
					request = new HttpGet(UrlService.BASE_URL + url + PATH);
					for (NameValuePair h : headers) {
						request.addHeader(h.getName(), h.getValue());
					}
				} else if (mode == MODE.POST) {
					request = new HttpPost(UrlService.BASE_URL + url);
					((HttpResponse) request).setEntity(new UrlEncodedFormEntity(nameValuePairs));
				}
				int timeoutConnection = 60000;
				HttpParams httpParameters = new BasicHttpParams();
				HttpConnectionParams.setConnectionTimeout(httpParameters, timeoutConnection);
				int timeoutSocket = 60000;
				HttpConnectionParams.setSoTimeout(httpParameters, timeoutSocket);
				HttpConnectionParams.setTcpNoDelay(httpParameters, true);
				if (mode == MODE.GET)
					mInputStream = executeURL(jsonObj, UrlService.BASE_URL + url + PATH, mode);
				else if (mode == MODE.POST)
					mInputStream = executeURL(jsonObj, UrlService.BASE_URL + url, mode);
				// HttpResponse response = httpClient.execute(request);
				// mInputStream = response.getEntity().getContent();
				mBufferedReader = new BufferedReader(new InputStreamReader(mInputStream));
				String line = null;
				mStringBuilder = new StringBuilder();
				while ((line = mBufferedReader.readLine()) != null) {
					mStringBuilder.append(line);
				}
				longMsg(mStringBuilder.toString());
				// response.getEntity().consumeContent();
				mInputStream.close();
			}
		} catch (Exception e) {
			System.out.println(e);
		} finally {
			try {
				if (mBufferedReader != null)
					mBufferedReader.close();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
		return mStringBuilder;
	}

	private void longMsg(String msg) {
		if (msg.length() > 4000) {
			System.out.println(msg); // or whatever you want
			longMsg(msg.substring(4000));
		} else {
			System.out.println(msg); // or whatever you want
		}
	}
}
