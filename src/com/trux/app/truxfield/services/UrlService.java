package com.trux.app.truxfield.services;

/**
 * Created by anurag.srivastava.
 */
public class UrlService {

	public static String BASE_URL = "http://staging.truxapp.com/trux";
	public static String LOGIN_AGENT = "/lgapi/loginAgents?";
	public static String SUB_CLINT = "/subapi/subclient?";
	public static String SUB_CLINT_DRIVER = "/subapi/driverMobileVehicleList?";
	public static String SEARCH_DRIVER = "apiv2/";
	public static String UPLOAD_DOCUMENT = "apiv2/driver/journey/updateclientdoc/";
	public static String UPLOAD_IMAGE = "/lgapi/uploadImage";
	public static String LOGIN_CRM_AUTHENTICATION = "/admin/register/userLogin?authentication=";
	public static String LATEST_VERSION = "http://crm.truxapp.com/truxapiv2/leasedvicecontroller/getappversion/eagle_eye";
	public static String LATEST_APK = "http://www.truxapp.com/appversion/eagleeye/TruxEagleEye.apk";
	public static String MANAGE_JOURNEY = "apiv2/leasedvicecontroller/getnonclosedbookings?";
	public static String ADD_DROP_POINTS = "apiv2/leasedvicecontroller/insertbookingstops";	
	public static String PUNCH_IN_OUT = "apiv2/leasedvicecontroller/driver/punch/";
	public static String START_KM = "apiv2/leasedvicecontroller/driver/punch/updatestartkm/";
	public static String END_KM = "apiv2/leasedvicecontroller/driver/punch/updateendkm/";
}
