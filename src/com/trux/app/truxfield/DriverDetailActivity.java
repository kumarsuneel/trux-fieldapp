package com.trux.app.truxfield;

import java.io.ByteArrayOutputStream;

import org.json.JSONException;
import org.json.JSONObject;

import com.trux.app.truxfield.interfaces.OnServiceResponseListener;
import com.trux.app.truxfield.model.GlobalVariable;
import com.trux.app.truxfield.model.ResponseCode;
import com.trux.app.truxfield.model.SharedPreferencesField;
import com.trux.app.truxfield.network.NetworkStateReceiver;
import com.trux.app.truxfield.services.AsyncTaskService;
import com.trux.app.truxfield.services.AsyncTaskService.MODE;
import com.trux.app.truxfield.services.UrlService;
import com.trux.truxagent.loader.ImageLoader;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences.Editor;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.Bitmap.CompressFormat;
import android.graphics.BitmapFactory;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.ColorDrawable;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.text.SpannableString;
import android.text.style.UnderlineSpan;
import android.util.DisplayMetrics;
import android.view.Display;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

public class DriverDetailActivity extends Activity implements OnClickListener, OnServiceResponseListener {

	private Button btnPunchIn, btnStartJourney, btnStopJourney, btnPunchOut, btnUploadDocument, btnPick, btnsubmit,
			btncancel, searchAgain;
	@SuppressWarnings("unused")
	private TextView textPersionName, textPhone, textVehicle, textClient, textMessage;
	private EditText etxtUsername;
	private ImageView imgDriver, imgPic;
	private ImageView mImageViewforImages[] = { imgPic };
	private Bitmap[] mBitmapforImages = new Bitmap[1];
	private JSONObject JsonValue, JsonDataValue;
	private SpannableString spanString;
	private StringBuilder mStringBuilder;
	private MainControllerApplication mMainControllerApplication;
	private ImageLoader mImageLoader;
	private SharedPreferencesField mSPLF;
	private String Result, Number, additionalProperties;
	private AlertDialog.Builder builder;
	private Dialog dialog;
	private static final int SCAN_AGAIN = 2;
	private static final int SELECTED_IMAGE = 1;
	private static final int cameraData = 0;
	private int mIndexforImages;
	private Display display;
	private DisplayMetrics metrics;
	private int width;
	private Intent mIntent;

	@Override
	public boolean onKeyDown(int keyCode, KeyEvent event) {
		if ((keyCode == KeyEvent.KEYCODE_BACK)) {
			mIntent = new Intent(DriverDetailActivity.this, TripManagement.class)
					.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP).addFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP);
			startActivity(mIntent);
			finish();
		}
		return super.onKeyDown(keyCode, event);
	}

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		getWindow().requestFeature(Window.FEATURE_ACTION_BAR);
		WindowManager wm = (WindowManager) getSystemService(Context.WINDOW_SERVICE);
		display = wm.getDefaultDisplay();
		metrics = new DisplayMetrics();
		display.getMetrics(metrics);
		width = metrics.widthPixels;
		mImageLoader = new ImageLoader(getApplicationContext());
		mSPLF = new SharedPreferencesField(getApplicationContext());
		mMainControllerApplication = (MainControllerApplication) getApplicationContext();
		Result = getIntent().getExtras().getString("Json");
		setContentView(R.layout.activity_driverdetail);
		imgDriver = (ImageView) findViewById(R.id.imgDriver);
		textPersionName = (TextView) findViewById(R.id.textPersionName);
		textPhone = (TextView) findViewById(R.id.textPhone);
		textVehicle = (TextView) findViewById(R.id.textVehicle);
		// textClient = (TextView)findViewById(R.id.textClient);
		btnPunchIn = (Button) findViewById(R.id.btnPunchIn);
		searchAgain = (Button) findViewById(R.id.searchAgain);
		btnStartJourney = (Button) findViewById(R.id.btnStartJourney);
		btnStopJourney = (Button) findViewById(R.id.btnStopJourney);
		btnPunchOut = (Button) findViewById(R.id.btnPunchOut);
		btnUploadDocument = (Button) findViewById(R.id.btnUploadDocument);
		inite();

		searchAgain.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				// mIntent = new Intent(DriverDetailActivity.this,
				// CreateOrderActivity.class)
				// .addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP).addFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP);
				// startActivity(mIntent);
				// finish();
				switch (v.getId()) {
				case R.id.searchAgain:
					try {
						Intent intent = new Intent("com.google.zxing.client.android.SCAN");
						intent.putExtra("SCAN_MODE", "QR_CODE_MODE,PRODUCT_MODE");
						startActivityForResult(intent, 2);
					} catch (Exception e) {
						e.printStackTrace();
						Toast.makeText(getApplicationContext(), "ERROR:" + e, Toast.LENGTH_LONG).show();
					}
					break;
				default:
					break;
				}
			}
		});

	}

	private void inite() {
		try {
			JsonValue = new JSONObject(Result);
			JsonDataValue = new JSONObject(JsonValue.getString("data"));

			mImageLoader.DisplayImage(JsonDataValue.getString("driverImage"), imgDriver, 4);
			Number = JsonDataValue.getString("driverPhoneNumber");
			spanString = new SpannableString(JsonDataValue.getString("driverName").equals("null")
					|| JsonDataValue.getString("driverName").equals("") ? "N/A"
							: JsonDataValue.getString("driverName"));
			spanString.setSpan(new UnderlineSpan(), 0, spanString.length(), 0);
			textPersionName.setText(spanString);
			textPhone.setText(getApplicationContext().getString(R.string.phone)
					+ (JsonDataValue.getString("driverPhoneNumber").equals("null")
							|| JsonDataValue.getString("driverPhoneNumber").equals("") ? "N/A"
									: JsonDataValue.getString("driverPhoneNumber")));
			textVehicle.setText(getApplicationContext().getString(R.string.vehicle_no)
					+ (JsonDataValue.getString("vehicleNumber").equals("null")
							|| JsonDataValue.getString("vehicleNumber").equals("") ? "N/A"
									: JsonDataValue.getString("vehicleNumber")));
			// textClient.setText(getApplicationContext().getString(R.string.client)+(JsonDataValue.getString("driverClientName").equals("null")||
			// JsonDataValue.getString("driverClientName").equals("") ? "N/A" :
			// JsonDataValue.getString("driverClientName")));

			if (JsonDataValue.getString("loginStatus").equalsIgnoreCase("0")) {
				btnPunchIn.setVisibility(View.VISIBLE);
				searchAgain.setVisibility(View.VISIBLE);
				btnPunchOut.setVisibility(View.GONE);
				btnStartJourney.setVisibility(View.GONE);
				btnStopJourney.setVisibility(View.GONE);
				btnUploadDocument.setVisibility(View.GONE);
			} else if (JsonDataValue.getString("loginStatus").equalsIgnoreCase("1")) {
				btnPunchIn.setVisibility(View.GONE);
				searchAgain.setVisibility(View.VISIBLE);
				if (JsonDataValue.getString("driverStatus").equalsIgnoreCase("0")) {
					btnPunchOut.setVisibility(View.VISIBLE);
					btnStartJourney.setVisibility(View.VISIBLE);
					btnStopJourney.setVisibility(View.GONE);
					btnUploadDocument.setVisibility(View.GONE);
					searchAgain.setVisibility(View.VISIBLE);
				} else if (JsonDataValue.getString("driverStatus").equalsIgnoreCase("1")) {
					btnStopJourney.setVisibility(View.VISIBLE);
					btnUploadDocument.setVisibility(View.VISIBLE);
					btnPunchOut.setVisibility(View.GONE);
					btnStartJourney.setVisibility(View.GONE);
					searchAgain.setVisibility(View.VISIBLE);
				}
			}
		} catch (JSONException e) {
			e.printStackTrace();
		}
	}

	public void onLogout(View v) {

		Intent intent = new Intent(this, LoginActivity.class);
		intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP);
		startActivity(intent);
		mSPLF.setBoolean(GlobalVariable.IS_LOGIN, false);
		mSPLF.clear();
		android.os.Process.killProcess(android.os.Process.myPid());
		Editor editor = getSharedPreferences("clear_cache", Context.MODE_PRIVATE).edit();
		editor.clear();
		editor.commit();
		finish();
	}

	@Override
	public void onClick(View v) {

		if (NetworkStateReceiver.isOnline(getApplicationContext())) {
			mStringBuilder = new StringBuilder();
			switch (v.getId()) {
			case R.id.btnPunchIn:
				mStringBuilder.append("driver").append("/").append("punch").append("/").append(Number).append("/")
						.append("1");
				new AsyncTaskService(this, UrlService.SEARCH_DRIVER, ResponseCode.PUNCHIN, MODE.POST,
						mStringBuilder.toString(), true).execute();
				break;
			case R.id.btnStartJourney:
				mStringBuilder.append("driver").append("/").append("journey").append("/").append(Number).append("/")
						.append("1");
				new AsyncTaskService(this, UrlService.SEARCH_DRIVER, ResponseCode.STARATJOURNEY, MODE.POST,
						mStringBuilder.toString(), true).execute();

				break;
			case R.id.btnStopJourney:
				mStringBuilder.append("driver").append("/").append("journey").append("/").append(Number).append("/")
						.append("0");
				new AsyncTaskService(this, UrlService.SEARCH_DRIVER, ResponseCode.STOPJOURNEY, MODE.POST,
						mStringBuilder.toString(), true).execute();
				break;
			case R.id.btnPunchOut:

				builder = new AlertDialog.Builder(this);
				builder.setTitle(getResources().getString(R.string.app_name))
						.setMessage(getResources().getString(R.string.info_punch_out)).setCancelable(false)
						.setIcon(R.drawable.alert_icon).setNegativeButton(getResources().getString(R.string.no),
								new DialogInterface.OnClickListener() {
									@Override
									public void onClick(DialogInterface dialogInterface, int i) {
										dialogInterface.dismiss();
									}
								})
						.setPositiveButton(getResources().getString(R.string.yes),
								new DialogInterface.OnClickListener() {
									@Override
									public void onClick(DialogInterface dialogInterface, int i) {
										try {
											mStringBuilder = new StringBuilder();
											mStringBuilder.append("driver").append("/").append("punch").append("/")
													.append(Number).append("/").append("0");
											new AsyncTaskService(DriverDetailActivity.this, UrlService.SEARCH_DRIVER,
													ResponseCode.PUNCHOUT, MODE.POST, mStringBuilder.toString(), true)
															.execute();
										} catch (Exception e) {

											e.printStackTrace();
										}
										dialogInterface.dismiss();
									}
								})
						.create().show();
				break;

			case R.id.btnUploadDocument:
				mUploadDocument();
				break;
			default:
				break;
			}
		} else {
			mMainControllerApplication.showError(getResources().getString(R.string.error_internet),
					R.drawable.repeating_bg_error);
		}
	}

	@SuppressLint("InflateParams")
	private void mUploadDocument() {
		dialog = new Dialog(this);
		dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
		LayoutInflater inflater = (LayoutInflater) getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		View view = inflater.inflate(R.layout.dilog_tripstarted, null, false);
		dialog.setCanceledOnTouchOutside(false);
		dialog.setContentView(view);
		dialog.getWindow().setLayout(width - width / 8, LinearLayout.LayoutParams.WRAP_CONTENT);
		dialog.getWindow().setBackgroundDrawable(new ColorDrawable(0));
		textMessage = (TextView) dialog.findViewById(R.id.textMessage);
		etxtUsername = (EditText) dialog.findViewById(R.id.etxtUsername);
		btnsubmit = (Button) dialog.findViewById(R.id.btnsubmit);
		btncancel = (Button) dialog.findViewById(R.id.btncancel);
		btnPick = (Button) dialog.findViewById(R.id.btnPick);
		imgPic = (ImageView) dialog.findViewById(R.id.imgPic);
		textMessage.setText(getResources().getString(R.string.trip_started_successfully_order_id_is) + "( "
				+ mSPLF.getString("OrederID", "") + " )");
		btnPick.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				builder = new AlertDialog.Builder(DriverDetailActivity.this);
				builder.setTitle("Select Image From");
				builder.setItems(new CharSequence[] { "Camera", "Gallery" }, new DialogInterface.OnClickListener() {
					@Override
					public void onClick(DialogInterface dialog, int which) {
						switch (which) {
						case 0:
							Intent i = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
							startActivityForResult(i, cameraData);
							builder.setCancelable(true);
							break;
						case 1:
							Intent j = new Intent(Intent.ACTION_PICK, MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
							startActivityForResult(j, SELECTED_IMAGE);
							builder.setCancelable(true);
						}
					}
				});
				builder.create().show();
				mIndexforImages = 0;
				mImageViewforImages[mIndexforImages] = imgPic;
			}
		});
		btnsubmit.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {

				try {
					ByteArrayOutputStream baos = new ByteArrayOutputStream();
					BitmapDrawable drawable = (BitmapDrawable) imgPic.getDrawable();
					Bitmap bitmap = drawable.getBitmap();
					bitmap.compress(CompressFormat.JPEG, 50, baos);
					byte[] data = baos.toByteArray();
					new AsyncTaskService(DriverDetailActivity.this, UrlService.UPLOAD_IMAGE, ResponseCode.UPLOADIMAGE,
							MODE.POST, new String(data), true).execute();
					dialog.dismiss();
				} catch (Exception e) {

					e.printStackTrace();
				}

			}
		});
		btncancel.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				dialog.dismiss();
			}
		});

		dialog.show();
	}

	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent Data) {
		super.onActivityResult(requestCode, resultCode, Data);
		switch (requestCode) {
		case cameraData:
			if (resultCode == RESULT_OK) {
				Bundle extras = Data.getExtras();
				mBitmapforImages[mIndexforImages] = (Bitmap) extras.get("data");
				mImageViewforImages[mIndexforImages].setImageBitmap(mBitmapforImages[mIndexforImages]);

			}
			break;
		case SELECTED_IMAGE:
			if (resultCode == RESULT_OK) {
				Uri uri = Data.getData();
				String[] Projection = { MediaStore.Images.Media.DATA };

				Cursor cursor = getContentResolver().query(uri, Projection, null, null, null);
				cursor.moveToFirst();

				int columnIndex = cursor.getColumnIndex(Projection[0]);
				String FilePath = cursor.getString(columnIndex);
				cursor.close();

				mBitmapforImages[mIndexforImages] = BitmapFactory.decodeFile(FilePath);
				// yourSelectedImage = getResizedBitmap(FilePath, MAX_SIZE);
				// Drawable d = new BitmapDrawable(yourSelectedImage);
				// mforPANCard = (ImageView)
				// findViewById(R.mMobileNumber.ivReturnedPic);
				mImageViewforImages[mIndexforImages].setImageBitmap(mBitmapforImages[mIndexforImages]);
			}
			break;
		case SCAN_AGAIN:
			if (resultCode == RESULT_OK) {

				if (NetworkStateReceiver.isOnline(getApplicationContext())) {
					try {
						mStringBuilder = new StringBuilder();
						mStringBuilder.append("driver").append("/")
								.append(Data.getStringExtra("SCAN_RESULT").replace("01-", ""));
						new AsyncTaskService(this, UrlService.SEARCH_DRIVER, ResponseCode.SEARCHDRIVER, MODE.POST,
								mStringBuilder.toString(), true).execute();
					} catch (Exception e) {
						e.printStackTrace();
					}
				} else {
					mMainControllerApplication.showError(getResources().getString(R.string.error_internet),
							R.drawable.repeating_bg_error);
				}

			} else if (resultCode == RESULT_CANCELED) {
				mMainControllerApplication.showError(getResources().getString(R.string.error_scan),
						R.drawable.repeating_bg_error);
			}
			break;

		}

	}

	public Bitmap getResizedBitmap(String selectedImage, int maxSize) {

		BitmapFactory.Options o = new BitmapFactory.Options();
		o.inJustDecodeBounds = true; //
		BitmapFactory.decodeFile(selectedImage, o);

		int width = o.outWidth;
		int height = o.outHeight;

		if (width < 1 || height < 1)
			return null;

		float bitmapRatio = (float) width / (float) height;
		if (bitmapRatio > 1) {
			width = maxSize;
			height = (int) (width / bitmapRatio);
		} else {
			height = maxSize;
			width = (int) (height * bitmapRatio);
		}

		o.inSampleSize = o.outWidth / width;
		o.inJustDecodeBounds = false;
		return BitmapFactory.decodeFile(selectedImage, o);
	}

	@Override
	public void onServiceResponse(StringBuilder result, int responseCode) {
		try {
			JSONObject resultJson = new JSONObject(result.toString());
			if (resultJson.getInt("errorCode") == 100) {
				if (responseCode == ResponseCode.PUNCHIN || responseCode == ResponseCode.STOPJOURNEY
						|| responseCode == ResponseCode.SEARCHDRIVER) {
					Result = resultJson.toString();
					inite();
				}
				if (responseCode == ResponseCode.STARATJOURNEY) {
					Result = resultJson.toString();
					JSONObject JsonDataValue = new JSONObject(new JSONObject(Result).getString("data"));
					additionalProperties = JsonDataValue.getString("additionalProperties").equals("null")
							|| JsonDataValue.getString("additionalProperties").equals("") ? "N/A"
									: JsonDataValue.getString("additionalProperties");
					JSONObject JsonPropertiesValue = new JSONObject(additionalProperties);
					mSPLF.setString("OrederID", JsonPropertiesValue.getString("orderId"));
					inite();
				}
				if (responseCode == ResponseCode.PUNCHOUT) {
					Intent mIntent = new Intent(DriverDetailActivity.this, MainActivity.class);
					startActivity(mIntent);
					DriverDetailActivity.this.finish();
				}
				if (responseCode == ResponseCode.UPLOADIMAGE) {
					JSONObject Jsondoc = new JSONObject();
					Jsondoc.put("orderId", mSPLF.getString("OrederID", ""))
							.put("clientOrderNumber", etxtUsername.getText().toString().trim())
							.put("clientOrderDocUrl", resultJson.getString("ImagePath"));
					new AsyncTaskService(DriverDetailActivity.this, UrlService.UPLOAD_DOCUMENT,
							ResponseCode.UPLOADDOCUMENT, MODE.POST, Jsondoc.toString(), true).execute();
				}
				if (responseCode == ResponseCode.UPLOADDOCUMENT) {
					((MainControllerApplication) getApplication()).showError("Successfully Uploded",
							R.drawable.repeating_bg_successful);
				}
			} else {
				((MainControllerApplication) getApplication()).showError(resultJson.getString("errorMesaage"),
						R.drawable.repeating_bg_error);
			}
		} catch (Exception e) {
			mMainControllerApplication.showError(getResources().getString(R.string.error_service),
					R.drawable.repeating_bg_error);
		}
	}

	@Override
	public void onServiceError() {
		((MainControllerApplication) getApplication()).showError(getResources().getString(R.string.error_service),
				R.drawable.repeating_bg_error);
	}

}
