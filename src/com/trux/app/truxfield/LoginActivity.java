package com.trux.app.truxfield;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.URL;
import java.util.List;
import java.util.Locale;

import org.apache.http.client.HttpClient;
import org.apache.http.client.ResponseHandler;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.BasicResponseHandler;
import org.apache.http.impl.client.DefaultHttpClient;
import org.json.JSONObject;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.trux.app.truxfield.model.GlobalVariable;
import com.trux.app.truxfield.model.SharedPreferencesField;
import com.trux.app.truxfield.network.NetworkStateReceiver;
import com.trux.app.truxfield.services.GPSTracker;
import com.trux.app.truxfield.services.UrlService;
import com.trux.truxagent.manager.AlertDialogManager;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager.NameNotFoundException;
import android.location.Address;
import android.location.Geocoder;
import android.net.Uri;
import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.support.v7.app.AlertDialog;
import android.text.method.HideReturnsTransformationMethod;
import android.text.method.PasswordTransformationMethod;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

@SuppressWarnings({ "deprecation", "unused" })
public class LoginActivity extends ActionBarActivity {
	// implements OnServiceResponseListener
	private TextView textappVersion;
	private String VersionName;
	private int VersionCode;
	private Button mLoginButton;
	private EditText etUsername, etPassword;
	private CheckBox cbShowPassword, cbRemindMe;
	private SharedPreferencesField mSPLF;
	private AlertDialogManager alert;
	private Intent mIntent;
	private StringBuilder mStringBuilder;
	private GPSTracker mGPSTracker;
	private String fromLocation;
	private StringBuilder response;
	String version_name;
	String version_code;
	private static final String TAG = "TruxEaglEye_v1.0.3";

	public static void showLog(String log) {
		Log.e(TAG, log);
	}

	public static void showToast(Context context, String show) {
		Toast.makeText(context, show, Toast.LENGTH_LONG).show();
	}

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_login);
		getSupportActionBar().setDisplayShowTitleEnabled(true);
		getSupportActionBar().hide();

		alert = new AlertDialogManager();
		mSPLF = new SharedPreferencesField(getApplicationContext());
		etUsername = (EditText) findViewById(R.id.etUsername);
		etPassword = (EditText) findViewById(R.id.etPassword);
		cbShowPassword = (CheckBox) findViewById(R.id.cbShwPwd);
		cbRemindMe = (CheckBox) findViewById(R.id.cbShwremind);
		mLoginButton = (Button) findViewById(R.id.bLogin);

		textappVersion = (TextView) findViewById(R.id.textappVersion);

		try {
			VersionName = this.getPackageManager().getPackageInfo(this.getPackageName(), 0).versionName;
			VersionCode = this.getPackageManager().getPackageInfo(this.getPackageName(), 0).versionCode;
			textappVersion.setText("v" + VersionName + "." + String.valueOf(VersionCode));
		} catch (NameNotFoundException e) {
			e.printStackTrace();
		}

		versionChecker();

		if (mSPLF.getBoolean("remindChecked", false)) {
			cbRemindMe.setChecked(true);
			etUsername.setText(mSPLF.getString("LoginName", ""));
			etPassword.setText(mSPLF.getString("LoginPassword", ""));
		}
		cbShowPassword.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
			@Override
			public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
				if (!isChecked) {
					etPassword.setTransformationMethod(new PasswordTransformationMethod());
					etPassword.setSelection(etPassword.length());
				} else {
					etPassword.setTransformationMethod(new HideReturnsTransformationMethod());
					etPassword.setSelection(etPassword.length());
				}
			}
		});
		cbRemindMe.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
			@Override
			public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
				if (isChecked) {
					mSPLF.setBoolean("remindChecked", true);
					mSPLF.setString("LoginName", etUsername.getText().toString().trim());
					mSPLF.setString("LoginPassword", etPassword.getText().toString().trim());
				} else {
					mSPLF.setBoolean("remindChecked", false);
					mSPLF.setString("LoginName", "");
					mSPLF.setString("LoginPassword", "");
				}
			}
		});

		final RequestQueue queue = Volley.newRequestQueue(this);

		mLoginButton.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View arg0) {
				// Get username, password from EditText
				mGPSTracker = new GPSTracker(LoginActivity.this);
				String username = etUsername.getText().toString();
				String password = etPassword.getText().toString();
				if (mGPSTracker.canGetLocation()) {

					String ADDRESS = getAddressFromLocation(mGPSTracker.getLatitude(), mGPSTracker.getLongitude());
					if (ADDRESS != null) {
						ADDRESS = ADDRESS.replaceAll(" ", "%20");
					} else {
						ADDRESS = "No Address found";
						ADDRESS = ADDRESS.replaceAll(" ", "%20");
					}

					if (cbRemindMe.isChecked()) {
						mSPLF.setString("LoginName", etUsername.getText().toString().trim());
						mSPLF.setString("LoginPassword", etPassword.getText().toString().trim());
					}
					if (mGPSTracker.getLatitude() != 0.0 && mGPSTracker.getLongitude() != 0.0) {
						// Check if username, password is filled
						if (username.trim().length() > 0 && password.trim().length() > 0) {
							if (!NetworkStateReceiver.isOnline(getApplicationContext())) {
								((MainControllerApplication) getApplication()).showError(
										getResources().getString(R.string.enable_internet),
										R.drawable.repeating_bg_error);
							} else {
								// Toast.makeText(LoginActivity.this, "Address=
								// " + ADDRESS,
								// Toast.LENGTH_LONG).show();
								mStringBuilder = new StringBuilder();
								mStringBuilder.append("username=" + etUsername.getText().toString().trim())
										.append("&password=" + etPassword.getText().toString())
										.append("&gcmId=" + "987654321")
										.append("&locationLat=" + mGPSTracker.getLatitude())
										.append("&locationLong=" + mGPSTracker.getLongitude())
										.append("&locationAddress=" + ADDRESS);
								mStringBuilder.toString();
								// new AsyncTaskService(LoginActivity.this,
								// UrlService.LOGIN_AGENT, ResponseCode.LOGIN,
								// MODE.POST, mStringBuilder.toString(),
								// true).execute();

								/* VOLLEY IMPLMENTATION */

								String url = UrlService.BASE_URL + UrlService.LOGIN_AGENT + mStringBuilder;
								LoginActivity.showLog("" + url);
								JsonObjectRequest jsObjRequest = new JsonObjectRequest(Request.Method.POST, url, null,
										new Response.Listener<JSONObject>() {

									@Override
									public void onResponse(JSONObject resultJson) {

										LoginActivity.showLog("" + resultJson);
										try {

											if (resultJson.getString("errorCode").equals("null")) {

												mSPLF.setBoolean(GlobalVariable.IS_LOGIN, true);
												mSPLF.setString("id", resultJson.getString("id"))
														.setString("email", resultJson.getString("email"))
														.setString("firstName", resultJson.getString("firstname"))
														.setString("lastName", resultJson.getString("lastname"))
														.setString("gcmId", resultJson.getString("gcmId"))
														.setString("phone", resultJson.getString("phoneNumber"))
														.setString("authentication",
																resultJson.getString("authentication"));
												mIntent = new Intent(LoginActivity.this, MainActivity.class)
														.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP)
														.addFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP);
												startActivity(mIntent);
												finish();

											} else {

												((MainControllerApplication) getApplication()).showError(
														"Incorrect Credentials", R.drawable.repeating_bg_error);
												LoginActivity.showToast(LoginActivity.this, "Incorrect Credentials");
											}

										} catch (Exception e) {

											LoginActivity.showLog("" + e);
											// Toast.makeText(LoginActivity.this,
											// (CharSequence) e,
											// Toast.LENGTH_LONG)
											// .show();
											LoginActivity.showToast(LoginActivity.this, "" + (CharSequence) e);
										}
									}
								}, new Response.ErrorListener() {

									@Override
									public void onErrorResponse(VolleyError error) {

										LoginActivity.showLog("" + error);
										// ((MainControllerApplication)
										// getApplication()).showError(getResources().getString(R.string.error_service),
										// R.drawable.repeating_bg_error);
										// Toast.makeText(LoginActivity.this,
										// (CharSequence) error,
										// Toast.LENGTH_LONG)
										// .show();
										LoginActivity.showToast(LoginActivity.this, "" + error);
									}
								});

								queue.add(jsObjRequest);

								/* VOLLEY IMPLMENTATION */

							}
						} else {
							alert.showAlertDialog(LoginActivity.this, "Login failed..",
									"Please enter username and password", false);
						}
					} else {
						alert.showAlertDialog(LoginActivity.this, "Login failed..",
								"Not find your location, Please login again few minutes later...", false);
					}

				} else {
					mGPSTracker.showSettingsAlert();
				}
			}
		});
	}

	/*
	 * @Override public void onServiceResponse(StringBuilder result, int
	 * responseCode) {
	 * 
	 * JSONObject resultJson = null; try { resultJson = new
	 * JSONObject(result.toString()); } catch (JSONException e1) { // TODO
	 * Auto-generated catch block e1.printStackTrace(); }
	 * 
	 * try {
	 * 
	 * if (resultJson.getString("errorCode").equals("null")) {
	 * 
	 * if (responseCode == ResponseCode.LOGIN) {
	 * mSPLF.setBoolean(GlobalVariable.IS_LOGIN, true); mSPLF.setString("id",
	 * resultJson.getString("id")).setString("email",
	 * resultJson.getString("email")) .setString("firstName",
	 * resultJson.getString("firstname")) .setString("lastName",
	 * resultJson.getString("lastname")) .setString("gcmId",
	 * resultJson.getString("gcmId")) .setString("phone",
	 * resultJson.getString("phoneNumber")) .setString("authentication",
	 * resultJson.getString("authentication")); mIntent = new
	 * Intent(LoginActivity.this, MainActivity.class)
	 * .addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP).addFlags(Intent.
	 * FLAG_ACTIVITY_SINGLE_TOP); startActivity(mIntent); finish();
	 * 
	 * } else { ((MainControllerApplication) getApplication())
	 * .showError(getResources().getString(R.string.error_no_data),
	 * R.drawable.repeating_bg_error); } } else { //
	 * Toast.makeText(getApplicationContext(), //
	 * resultJson.getString("errorMessage"), // Toast.LENGTH_SHORT).show();
	 * ((MainControllerApplication) getApplication()).showError(
	 * "Incorrect Credentials", R.drawable.repeating_bg_error); } // if
	 * (resultJson.getString("errorCode").equals("E053"))
	 * 
	 * } catch (Exception e) { // ((MainControllerApplication) getApplication())
	 * // .showError(getResources().getString(R.string.error_service), //
	 * R.drawable.repeating_bg_error);
	 * 
	 * Toast.makeText(LoginActivity.this, (CharSequence) e,
	 * Toast.LENGTH_LONG).show(); }
	 * 
	 * }
	 * 
	 * @Override public void onServiceError() {
	 * 
	 * ((MainControllerApplication)
	 * getApplication()).showError(getResources().getString(R.string.
	 * error_service), R.drawable.repeating_bg_error); }
	 */

	// private String getCompleteAddressString(double LATITUDE, double
	// LONGITUDE) {
	// String strAdd = "";
	// Geocoder geocoder = new Geocoder(this, Locale.getDefault());
	// try {
	// List<Address> addresses = geocoder.getFromLocation(LATITUDE, LONGITUDE,
	// 1);
	// if (addresses != null) {
	// Address returnedAddress = addresses.get(0);
	// StringBuilder strReturnedAddress = new StringBuilder("");
	//
	// for (int i = 0; i < returnedAddress.getMaxAddressLineIndex(); i++) {
	// strReturnedAddress.append(returnedAddress.getAddressLine(i)).append("\n");
	// }
	// strAdd = strReturnedAddress.toString();
	//// Log.w("My Current loction address", "" +
	// strReturnedAddress.toString());
	// } else {
	//// Log.w("My Current loction address", "No Address returned!");
	// }
	// } catch (Exception e) {
	// e.printStackTrace();
	//// Log.w("My Current loction address", "Canont get Address!");
	// }
	// return strAdd;
	// }

	public void versionChecker() {
		final RequestQueue queue = Volley.newRequestQueue(this);

		/* VOLLEY IMPLMENTATION */

		String url = UrlService.LATEST_VERSION;
		LoginActivity.showLog("" + url);
		JsonObjectRequest jsObjRequest = new JsonObjectRequest(Request.Method.GET, url, null,
				new Response.Listener<JSONObject>() {

					@Override
					public void onResponse(JSONObject resultJson) {

						LoginActivity.showLog("" + resultJson);
						try {

							if (resultJson.getInt("errorCode") == 100) {
								JSONObject jsObjRequestData = new JSONObject();
								jsObjRequestData = resultJson.getJSONObject("data");

								if (jsObjRequestData.getBoolean("is_active")) {
									version_name = jsObjRequestData.getString("version_name");
									version_code = jsObjRequestData.getString("version_code");
									LoginActivity
											.showLog("version_name " + version_name + "  version_code " + version_code);
									versionCheck(version_name, version_code);
								}

							} else {

							}

						} catch (Exception e) {
							LoginActivity.showLog("" + e);
							LoginActivity.showToast(LoginActivity.this, "" + e);
						}
					}
				}, new Response.ErrorListener() {

					@Override
					public void onErrorResponse(VolleyError error) {

						LoginActivity.showLog("" + error);
						LoginActivity.showToast(LoginActivity.this, "" + error);
					}
				});

		queue.add(jsObjRequest);

		/* VOLLEY IMPLMENTATION */

		// } catch (MalformedURLException e) { // } catch (IOException e) { // }

	}

	private void versionCheck(String version_name, String version_code) {
		String versionServer = version_name + "." + version_code;
		String versionApp = VersionName + "." + VersionCode;

		try {
			if (!(versionServer.equals(versionApp))) {
				AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(this);
				// set title
				alertDialogBuilder.setTitle("Version Checker"); // set dialog
																// message
				alertDialogBuilder
						.setMessage("Your app is out dated!!!\nClick on download now to download latest verion")
						.setCancelable(false)
						.setPositiveButton("Download Later", new DialogInterface.OnClickListener() {
							public void onClick(DialogInterface dialog, int id) { // if
																					// this
																					// button
																					// is
																					// clicked,
																					// close
																					// //
																					// current
																					// activity
								finish();
							}
						}).setNegativeButton("Download Now", new DialogInterface.OnClickListener() {
							public void onClick(DialogInterface dialog, int id) { // if
																					// this
																					// button
																					// is
																					// clicked,
																					// just
																					// close
																					// //
																					// the
																					// dialog
																					// box
																					// and
																					// do
																					// nothing
								Uri uri = Uri
										.parse("http://www.truxapp.com/appversion/eagleeye/TruxEagleEye_v1.0.2.apk");
								// missing 'http://' will cause crashed
								Intent intent = new Intent(Intent.ACTION_VIEW, uri);
								startActivity(intent);
							}
						}); // create alert dialog
				AlertDialog alertDialog = alertDialogBuilder.create(); // show
																		// it
				alertDialog.show();
			} else {
				return;
			}
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	// public class GeocoderHandler extends Handler {
	// public void handleMessage(Message message) {
	// switch (message.what) {
	// case 1:
	// Bundle bundle = message.getData();
	// fromLocation = bundle.getString("address");
	//
	// // mSPLF.setString("address", fromLocation);
	// break;
	// default:
	//
	// }
	// // Log.d("",fromLocation);
	// }
	// }

	//
	public String getAddressFromLocation(final double latitude, final double longitude) {
		String result = null;
		// Thread thread = new Thread() {
		// @Override
		// public void run() {
		Geocoder geocoder = new Geocoder(LoginActivity.this, Locale.getDefault());

		try {
			List<Address> addressList = geocoder.getFromLocation(latitude, longitude, 1);
			if (addressList != null && addressList.size() > 0) {
				Address address = addressList.get(0);
				StringBuilder sb = new StringBuilder();
				for (int i = 0; i < address.getMaxAddressLineIndex(); i++) {
					sb.append(address.getAddressLine(i)).append(" ");
				}
				sb.append(address.getLocality()).append(" ");
				sb.append(address.getPostalCode()).append(" ");
				sb.append(address.getCountryName());
				result = sb.toString();

			}
		} catch (IOException e) {

		}
		// finally {
		// Message message = Message.obtain();
		// message.setTarget(handler);
		// if (result != null) {
		// message.what = 1;
		// Bundle bundle = new Bundle();
		// result = "Latitude: " + latitude + " Longitude: " + longitude +
		// "\n\nAddress:\n" + result;
		// bundle.putString("address", result);
		// message.setData(bundle);
		// } else {
		// message.what = 1;
		// Bundle bundle = new Bundle();
		// result = "Latitude: " + latitude + " Longitude: " + longitude
		// + "\n Unable to get address for this lat-long.";
		// bundle.putString("address", result);
		// message.setData(bundle);
		// }
		// message.sendToTarget();
		// }
		// }
		// };
		// thread.start();
		return result;
	}

	public String getAddress(Context ctx, double latitude, double longitude) {
		StringBuilder result = new StringBuilder();
		try {
			Geocoder geocoder = new Geocoder(ctx, Locale.getDefault());
			List<Address> addresses = geocoder.getFromLocation(latitude, longitude, 1);
			if (addresses.size() > 0) {
				Address address = addresses.get(0);

				String locality = address.getLocality();
				String city = address.getCountryName();
				String region_code = address.getCountryCode();
				String zipcode = address.getPostalCode();
				double lat = address.getLatitude();
				double lon = address.getLongitude();

				result.append(locality + " ");
				result.append(city + " " + region_code + " ");
				result.append(zipcode);

			}
		} catch (IOException e) {
			Log.e("tag", e.getMessage());
		}

		return result.toString();
	}

	public String getLocation(Context context, double latitude, double longitude) {
		Geocoder geocoder;
		List<Address> addresses = null;
		geocoder = new Geocoder(this, Locale.getDefault());

		try {
			addresses = geocoder.getFromLocation(latitude, longitude, 1);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return addresses.get(0).toString();

	}
}
