package com.trux.app.truxfield;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.util.ArrayList;
import java.util.Calendar;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import com.amazonaws.auth.AWSCredentials;
import com.amazonaws.auth.BasicAWSCredentials;
import com.amazonaws.mobileconnectors.s3.transferutility.TransferListener;
import com.amazonaws.mobileconnectors.s3.transferutility.TransferObserver;
import com.amazonaws.mobileconnectors.s3.transferutility.TransferState;
import com.amazonaws.mobileconnectors.s3.transferutility.TransferUtility;
import com.amazonaws.services.s3.AmazonS3;
import com.amazonaws.services.s3.AmazonS3Client;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonArrayRequest;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.squareup.picasso.Picasso;
import com.trux.app.truxfield.interfaces.OnServiceResponseListener;
import com.trux.app.truxfield.model.Driver;
import com.trux.app.truxfield.model.DropoffPoints;
import com.trux.app.truxfield.model.ResponseCode;
import com.trux.app.truxfield.model.SharedPreferencesField;
import com.trux.app.truxfield.model.UploadImage;
import com.trux.app.truxfield.network.NetworkStateReceiver;
import com.trux.app.truxfield.services.AsyncTaskService;
import com.trux.app.truxfield.services.AsyncTaskService.MODE;
import com.trux.app.truxfield.services.UrlService;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.ColorDrawable;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.util.Base64;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.Display;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemSelectedListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

public class DriverTripDetailActivity extends Activity
		implements OnServiceResponseListener, OnClickListener, OnItemSelectedListener {
	private Button btnScanAgain;
	private Button btnStopJourney;
	private Button btnUploadDocument;
	private int mIndexforImages;
	private Button btnAddDropPoints;
	private Button btnStartJourney;
	private Button btnPunchOut;
	private ImageView imgDocOne, imgDocTwo, imgDocTreee, imgDocFour;
	private StringBuilder mStringBuilder;
	private MainControllerApplication mMainControllerApplication;
	private SharedPreferencesField mSPLF;
	private Intent mIntent;
	private Bundle msavedInstanceState;
	private ArrayList<String> imagePath = new ArrayList<String>();
	private ImageView mImageViewforImages[] = { imgDocOne, imgDocTwo, imgDocTreee, imgDocFour };
	private Bitmap[] mBitmapforImages = new Bitmap[4];

	private static final int SCAN_AGAIN = 2;
	private static final int SELECTED_IMAGE = 1;
	private static final int cameraData = 0;
	private ImageView imgDriver;
	private TextView textPersionName;
	private TextView textPhone;
	private TextView textVehicle;
	private LinearLayout stopJourneyLL;
	private LinearLayout startJourneyLL;
	private static Dialog dialog;
	private int width;
	private Display display;
	private DisplayMetrics metrics;
	private AlertDialog.Builder builder;
	private String jsonData;
	private Driver driver;
	private String _path;
	private EditText etxtUsername;
	int counter;
	boolean isDialogShowing = false;
	private View hiddenInfo;
	private EditText dropPointsET;
	private EditText noOfBoxes;
	private TextView textMessage;

	@Override
	public boolean onKeyDown(int keyCode, KeyEvent event) {
		if ((keyCode == KeyEvent.KEYCODE_BACK)) {
			mIntent = new Intent(DriverTripDetailActivity.this, TripManagement.class)
					.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP).addFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP);
			startActivity(mIntent);
			finish();
		}
		return super.onKeyDown(keyCode, event);
	}

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		msavedInstanceState = savedInstanceState;
		super.onCreate(savedInstanceState);
		getWindow().requestFeature(Window.FEATURE_ACTION_BAR);
		WindowManager wm = (WindowManager) getSystemService(Context.WINDOW_SERVICE);
		display = wm.getDefaultDisplay();
		metrics = new DisplayMetrics();
		display.getMetrics(metrics);
		width = metrics.widthPixels;
		mSPLF = new SharedPreferencesField(getApplicationContext());
		mMainControllerApplication = (MainControllerApplication) getApplicationContext();
		setContentView(R.layout.activity_driver_trip_detail);
		initUI();
		int fromActivity = getIntent().getExtras().getInt("from_activity", 0);
		if (fromActivity == 0) {
			jsonData = getIntent().getExtras().getString("Json");
			parseJsonData(jsonData);
		} else if (fromActivity == 1) {
			driver = (Driver) getIntent().getSerializableExtra("driver_detail");
			setUI();

		}
	}

	private void setUI() {
		startJourneyLL.setVisibility(View.GONE);
		stopJourneyLL.setVisibility(View.VISIBLE);

		textPhone.setText(getApplicationContext().getString(R.string.phone) + driver.getDriverPhoneNumber());
		textPersionName.setText(driver.getDriverName());
		textVehicle.setText(getApplicationContext().getString(R.string.vehicle_no) + driver.getVehicleNumber());
		Picasso.with(this) //
				.load(driver.driverImage) //
				.placeholder(R.drawable.ic_launcher)

				.into(imgDriver);

	}

	private void parseJsonData(String jsonData) {
		try {
			JSONObject JsonValue = new JSONObject(jsonData);
			JSONObject jsonObject = JsonValue.getJSONObject("data");
			LoginActivity.showLog("" + jsonObject);
			driver = new Driver();
			driver.setLoginStatus(jsonObject.getString("loginStatus"));
			driver.setVehicleType(jsonObject.getString("vehicleType"));
			driver.setVehicleNumber(jsonObject.getString("vehicleNumber"));
			driver.setDriverStatus(jsonObject.getString("driverStatus"));
			driver.setDriverName(jsonObject.getString("driverName"));
			driver.setDriverId(jsonObject.getString("driverId"));
			driver.setDriverPhoneNumber(jsonObject.getString("driverPhoneNumber"));

			textPhone.setText(getApplicationContext().getString(R.string.phone) + driver.getDriverPhoneNumber());
			textPersionName.setText(driver.getDriverName());
			textVehicle.setText(getApplicationContext().getString(R.string.vehicle_no) + driver.getVehicleNumber());

			if (driver.getLoginStatus().equalsIgnoreCase("0")) {
				startJourneyLL.setVisibility(View.VISIBLE);
				stopJourneyLL.setVisibility(View.GONE);
				addStartEndKM("Input start KM");
			} else if (driver.getLoginStatus().equalsIgnoreCase("1")) {

				if (driver.getDriverStatus().equalsIgnoreCase("0")) {
					startJourneyLL.setVisibility(View.VISIBLE);
					stopJourneyLL.setVisibility(View.GONE);
				} else if (driver.getDriverStatus().equalsIgnoreCase("1")) {
					startJourneyLL.setVisibility(View.GONE);
					stopJourneyLL.setVisibility(View.VISIBLE);
				}
			}
		} catch (JSONException e) {
			e.printStackTrace();
		}

	}

	private void initUI() {
		btnScanAgain = (Button) findViewById(R.id.btnScanAgain);
		btnStopJourney = (Button) findViewById(R.id.btnStopJourney);
		btnUploadDocument = (Button) findViewById(R.id.btnUploadDocument);
		btnAddDropPoints = (Button) findViewById(R.id.btnAddDropPoints);
		btnStartJourney = (Button) findViewById(R.id.btnStartJourney);
		btnPunchOut = (Button) findViewById(R.id.btnPunchOut);
		imgDriver = (ImageView) findViewById(R.id.imgDriver);
		textPersionName = (TextView) findViewById(R.id.textPersionName);
		textPhone = (TextView) findViewById(R.id.textPhone);
		textVehicle = (TextView) findViewById(R.id.textVehicle);
		stopJourneyLL = (LinearLayout) findViewById(R.id.stopJourneyLL);
		startJourneyLL = (LinearLayout) findViewById(R.id.startJourneyLL);
		btnScanAgain.setOnClickListener(this);
		btnStopJourney.setOnClickListener(this);
		btnUploadDocument.setOnClickListener(this);
		btnAddDropPoints.setOnClickListener(this);
		btnStartJourney.setOnClickListener(this);
		btnPunchOut.setOnClickListener(this);

	}

	@Override
	public void onClick(View v) {
		switch (v.getId()) {
		case R.id.btnScanAgain:
			scanAgain();
			break;
		case R.id.btnStopJourney:
			stopJourney();
			break;
		case R.id.btnUploadDocument:

			uploadDoucument();

			break;
		case R.id.btnAddDropPoints:
			addDropPoints();

			break;
		case R.id.btnStartJourney:
			startJourney();
			break;
		case R.id.btnPunchOut:
			punchOut();
			break;
		}

	}

	private void startJourney() {
		mStringBuilder = new StringBuilder();

		mStringBuilder.append("apiv2/driver").append("/").append("journey").append("/")
				.append(driver.getDriverPhoneNumber()).append("/").append("1");
		String url = UrlService.BASE_URL + mStringBuilder;
		LoginActivity.showLog("" + url);
		final RequestQueue queue = Volley.newRequestQueue(DriverTripDetailActivity.this);
		JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(Request.Method.POST, url, null,
				new Response.Listener<JSONObject>() {

					@Override
					public void onResponse(JSONObject resultJson) {
						startJourneyLL.setVisibility(View.GONE);
						stopJourneyLL.setVisibility(View.VISIBLE);
						LoginActivity.showLog("" + resultJson);

						JSONObject JsonDataValue = new JSONObject();

						try {
							JsonDataValue = resultJson.getJSONObject("data");
							// = new JSONObject();
							// JSONObject JsonPropertiesValue =
							// JsonDataValue.getJSONObject("additionalProperties");
							mSPLF.setString("OrederID",
									JsonDataValue.getJSONObject("additionalProperties").getString("OrederID"));
						} catch (JSONException e) {

							e.printStackTrace();
						}
					}
				}, new Response.ErrorListener() {

					@Override
					public void onErrorResponse(VolleyError error) {

						LoginActivity.showLog("" + error);

					}
				});

		queue.add(jsonObjectRequest);
	}

	private void stopJourney() {
		mStringBuilder = new StringBuilder();

		mStringBuilder.append("apiv2/driver").append("/").append("journey").append("/")
				.append(driver.getDriverPhoneNumber()).append("/").append("0");

		final RequestQueue queue = Volley.newRequestQueue(DriverTripDetailActivity.this);
		JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(Request.Method.POST,
				UrlService.BASE_URL + mStringBuilder, null, new Response.Listener<JSONObject>() {

					@Override
					public void onResponse(JSONObject resultJson) {
						switchToTripManagement();
					}

				}, new Response.ErrorListener() {

					@Override
					public void onErrorResponse(VolleyError error) {

						LoginActivity.showLog("" + error);

						LoginActivity.showToast(DriverTripDetailActivity.this, "" + error);
					}
				});

		queue.add(jsonObjectRequest);
	}

	private void switchToTripManagement() {
		Intent intent = new Intent(DriverTripDetailActivity.this, TripManagement.class);
		startActivity(intent);
		finish();
	}

	private void uploadDoucument() {
		uploadClienDocument();

	}

	private void captureImageDialog(int index, ImageView imgPic) {
		builder = new AlertDialog.Builder(DriverTripDetailActivity.this);
		builder.setTitle("Select Image From");
		builder.setItems(new CharSequence[] { "Camera", "Gallery" }, new DialogInterface.OnClickListener() {

			@Override
			public void onClick(DialogInterface dialog, int which) {
				switch (which) {
				case 0:
					Intent i = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
					_path = Environment.getExternalStorageDirectory() + "/" + Calendar.getInstance().getTimeInMillis()
							+ ".png";
					File file = new File(_path);
					Uri outputFileUri = Uri.fromFile(file);
					i.putExtra(MediaStore.EXTRA_OUTPUT, outputFileUri);
					startActivityForResult(i, cameraData);
					builder.setCancelable(true);
					break;
				case 1:
					Intent j = new Intent(Intent.ACTION_PICK, MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
					startActivityForResult(j, SELECTED_IMAGE);
					builder.setCancelable(true);
				}
			}
		});
		builder.create().show();
		mIndexforImages = index;
		mImageViewforImages[mIndexforImages] = imgPic;
	}

	private void addDropPoints() {
		initDialogDropPoint();
	}

	private void scanAgain() {
		try {
			Intent intent = new Intent("com.google.zxing.client.android.SCAN");
			intent.putExtra("SCAN_MODE", "QR_CODE_MODE,PRODUCT_MODE");
			startActivityForResult(intent, 2);
		} catch (Exception e) {
			e.printStackTrace();
			Toast.makeText(getApplicationContext(), "ERROR:" + e, Toast.LENGTH_LONG).show();
		}
	}

	private void punchOut() {
		addStartEndKM("Input End KM");
	}

	private void initDialogDropPoint() {
		counter = 0;
		final ArrayList<DropoffPoints> listOfDropPoints = new ArrayList<DropoffPoints>();
		dialog = null;
		dialog = new Dialog(this);
		dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
		LayoutInflater inflater = (LayoutInflater) getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		View view = inflater.inflate(R.layout.dialod_drop_points, null, false);
		dialog.setCanceledOnTouchOutside(false);
		dialog.setContentView(view);
		dialog.getWindow().setLayout(width - width / 8, LinearLayout.LayoutParams.WRAP_CONTENT);
		dialog.getWindow().setBackgroundDrawable(new ColorDrawable(0));

		LinearLayout addDropPointsLL = (LinearLayout) dialog.findViewById(R.id.addDropPointsLL);
		Button btnAddMore = (Button) dialog.findViewById(R.id.btnAddMore);
		Button btnSubmit = (Button) dialog.findViewById(R.id.btnsubmit);

		hiddenInfo = getLayoutInflater().inflate(R.layout.dynamic_edit_text_view, addDropPointsLL, false);
		dropPointsET = (EditText) hiddenInfo.findViewById(R.id.dropPointsET);
		noOfBoxes = (EditText) hiddenInfo.findViewById(R.id.noOfBoxes);
		addDropPointsLL.addView(hiddenInfo);
		btnAddMore.setTag(addDropPointsLL);
		btnSubmit.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				if (!dropPointsET.getText().toString().isEmpty() && !noOfBoxes.getText().toString().isEmpty()
						&& driver != null && counter == 0) {
					counter++;
					DropoffPoints dropoffPoints = new DropoffPoints();
					dropoffPoints.dropBookingId = driver.getBookingLeaseId();
					dropoffPoints.dropLocation = dropPointsET.getText().toString();
					dropoffPoints.dropedBoxes = noOfBoxes.getText().toString();
					listOfDropPoints.add(dropoffPoints);

				}
				sendDropPointsToServer(listOfDropPoints, dialog);

			}

		});

		btnAddMore.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				LoginActivity
						.showLog("  dropPointsET.getText().toString().isEmpty()  " + dropPointsET.getText().toString()
								+ "noOfBoxes.getText().toString()" + noOfBoxes.getText().toString());
				if (!dropPointsET.getText().toString().isEmpty() && !noOfBoxes.getText().toString().isEmpty()
						&& driver != null) {
					DropoffPoints dropoffPoints = new DropoffPoints();
					dropoffPoints.dropBookingId = driver.getBookingLeaseId();
					dropoffPoints.dropLocation = dropPointsET.getText().toString();
					dropoffPoints.dropedBoxes = noOfBoxes.getText().toString();
					listOfDropPoints.add(dropoffPoints);
					LinearLayout addDropPointsLL = (LinearLayout) v.getTag();
					hiddenInfo = getLayoutInflater().inflate(R.layout.dynamic_edit_text_view, addDropPointsLL, false);
					dropPointsET = (EditText) hiddenInfo.findViewById(R.id.dropPointsET);
					noOfBoxes = (EditText) hiddenInfo.findViewById(R.id.noOfBoxes);
					addDropPointsLL.addView(hiddenInfo);
				} else {
					Toast.makeText(DriverTripDetailActivity.this, "Add Drop Location and no of boxes",
							Toast.LENGTH_LONG).show();
				}

			}
		});
		if (dialog != null && !isFinishing())
			dialog.show();

	}

	private void addStartEndKM(String inputType) {
		dialog = null;
		dialog = new Dialog(this);
		dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
		LayoutInflater inflater = (LayoutInflater) getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		View view = inflater.inflate(R.layout.dialog_input_distance, null, false);
		dialog.setCanceledOnTouchOutside(false);
		dialog.setContentView(view);
		dialog.getWindow().setLayout(width - width / 8, LinearLayout.LayoutParams.WRAP_CONTENT);
		dialog.getWindow().setBackgroundDrawable(new ColorDrawable(0));
		EditText etxtUsername = (EditText) dialog.findViewById(R.id.etxtUsername);
		etxtUsername.setHint(inputType);

		Button btnSubmit = (Button) dialog.findViewById(R.id.btnsubmit);

		btnSubmit.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {

				mStringBuilder = new StringBuilder();

				if (driver.getDriverStatus().equals("1")) {
					mStringBuilder.append(driver.getDriverId()).append("/").append("0");
				} else {
					mStringBuilder.append(driver.getDriverId()).append("/").append("1");
				}

				final RequestQueue queue = Volley.newRequestQueue(DriverTripDetailActivity.this);
				JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(Request.Method.POST,
						UrlService.BASE_URL + UrlService.PUNCH_IN_OUT + mStringBuilder, null,
						new Response.Listener<JSONObject>() {

					@Override
					public void onResponse(JSONObject resultJson) {
						LoginActivity.showLog("" + resultJson);
						if (driver.getDriverStatus().equals("1")) {
							switchToTripManagement();
						} else {
							dialog.dismiss();
						}
					}
				}, new Response.ErrorListener() {

					@Override
					public void onErrorResponse(VolleyError error) {

						LoginActivity.showLog("" + error);

						LoginActivity.showToast(DriverTripDetailActivity.this, "" + error);
					}
				});

				queue.add(jsonObjectRequest);

			}

		});
		if (dialog != null && !isFinishing()) {

			dialog.show();
		}
	}

	private void uploadClienDocument() {
		imagePath.clear();
		dialog = null;
		dialog = new Dialog(this);
		dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
		LayoutInflater inflater = (LayoutInflater) getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		View view = inflater.inflate(R.layout.dialog_upload_doc, null, false);
		dialog.setCanceledOnTouchOutside(false);
		dialog.setContentView(view);
		dialog.getWindow().setLayout(width - width / 8, LinearLayout.LayoutParams.WRAP_CONTENT);
		dialog.getWindow().setBackgroundDrawable(new ColorDrawable(0));
		textMessage = (TextView) dialog.findViewById(R.id.textMessage);
		etxtUsername = (EditText) dialog.findViewById(R.id.etxtUsername);
		Button btnsubmit = (Button) dialog.findViewById(R.id.btnsubmit);
		Button btncancel = (Button) dialog.findViewById(R.id.btncancel);

		imgDocOne = (ImageView) dialog.findViewById(R.id.imgDocOne);
		imgDocTwo = (ImageView) dialog.findViewById(R.id.imgDocTwo);
		imgDocTreee = (ImageView) dialog.findViewById(R.id.imgDocTreee);
		imgDocFour = (ImageView) dialog.findViewById(R.id.imgDocFour);

		imgDocOne.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				captureImageDialog(0, (ImageView) v);
			}

		});
		imgDocTwo.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				captureImageDialog(1, (ImageView) v);
			}

		});
		imgDocTreee.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				captureImageDialog(2, (ImageView) v);
			}

		});
		imgDocFour.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				captureImageDialog(3, (ImageView) v);
			}

		});
		btnsubmit.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {

				try {
					if (etxtUsername.getText().toString().isEmpty()) {
						Toast.makeText(DriverTripDetailActivity.this, "Please Enter Order id", Toast.LENGTH_LONG)
								.show();
					} else {
						uploadImages();
						dialog.dismiss();
					}
				} catch (Exception e) {

					e.printStackTrace();
				}

			}

		});
		btncancel.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				dialog.dismiss();
			}
		});
		try {
			if (dialog != null && !isFinishing())

				dialog.show();
		} catch (Throwable e) {
			e.printStackTrace();
		}
	}

	public String getStringImage(Bitmap bmp) {
		ByteArrayOutputStream baos = new ByteArrayOutputStream();
		bmp.compress(Bitmap.CompressFormat.JPEG, 100, baos);
		byte[] imageBytes = baos.toByteArray();
		String encodedImage = Base64.encodeToString(imageBytes, Base64.DEFAULT);
		return encodedImage;
	}

	private void uploadImages() {

		final RequestQueue queue = Volley.newRequestQueue(this);
		try {

			JSONObject Jsondoc = new JSONObject();
			String orredrid = mSPLF.getString("OrederID", "");
			Jsondoc.put("orderId", orredrid);
			textMessage.setText(orredrid);
			Jsondoc.put("clientOrderNumber", etxtUsername.getText().toString().trim());
			int docCounter = 0;
			for (int i = 0; i < imagePath.size(); i++) {

				if (i == 0) {
					docCounter++;
					Jsondoc.put("clientOrderDocUrl", imagePath.get(i));
				} else {
					docCounter++;
					Jsondoc.put("clientOrderDocUrl" + docCounter, imagePath.get(i));
				}

			}

			String url = UrlService.BASE_URL + UrlService.UPLOAD_DOCUMENT;
			LoginActivity.showLog("" + url);
			JsonObjectRequest jsObjRequest = new JsonObjectRequest(Request.Method.POST, url, Jsondoc,
					new Response.Listener<JSONObject>() {

						@Override
						public void onResponse(JSONObject resultJson) {

							LoginActivity.showLog("" + resultJson);
							try {

								if (resultJson.getInt("errorCode") == 100) {

									((MainControllerApplication) getApplication()).showError("Uploaded successfully",
											R.drawable.repeating_bg_error);
									finish();

								} else {

									((MainControllerApplication) getApplication()).showError(
											"Something went wrong...please try again...!!!",
											R.drawable.repeating_bg_error);
									// LoginActivity.showToast(DriverTripDetailActivity.this,
									// "Incorrect Credentials");
								}

							} catch (Exception e) {

								LoginActivity.showLog("" + e);
								LoginActivity.showToast(DriverTripDetailActivity.this, "" + (CharSequence) e);
							}
						}
					}, new Response.ErrorListener() {

						@Override
						public void onErrorResponse(VolleyError error) {

							LoginActivity.showLog("" + error);
							((MainControllerApplication) getApplication()).showError(
									getResources().getString(R.string.error_service), R.drawable.repeating_bg_error);
							LoginActivity.showToast(DriverTripDetailActivity.this, "" + error);
						}
					});

			queue.add(jsObjRequest);

			/* VOLLEY IMPLMENTATION */
		} catch (JSONException e) {

			e.printStackTrace();
		}

	}

	public void onActivityResult(int requestCode, int resultCode, Intent intent) {

		switch (requestCode) {
		case cameraData:
			if (resultCode == RESULT_OK) {
				// Bundle extras = intent.getExtras();

				// mBitmapforImages[mIndexforImages] = (Bitmap)
				// extras.get("data");
				// mImageViewforImages[mIndexforImages].setImageBitmap(mBitmapforImages[mIndexforImages]);
				UploadToS3Bucket upload = new UploadToS3Bucket();
				upload.execute(_path);
			}
			break;
		case SELECTED_IMAGE:
			if (resultCode == RESULT_OK) {
				Uri uri = intent.getData();
				String[] Projection = { MediaStore.Images.Media.DATA };
				Cursor cursor = getContentResolver().query(uri, Projection, null, null, null);
				cursor.moveToFirst();
				int columnIndex = cursor.getColumnIndex(Projection[0]);
				_path = cursor.getString(columnIndex);
				cursor.close();

				UploadToS3Bucket upload = new UploadToS3Bucket();
				upload.execute(_path);

			}
			break;
		case SCAN_AGAIN:
			if (resultCode == RESULT_OK) {

				if (NetworkStateReceiver.isOnline(getApplicationContext())) {
					try {
						mStringBuilder = new StringBuilder();
						mStringBuilder.append("driver").append("/")
								.append(intent.getStringExtra("SCAN_RESULT").replace("01-", ""));
						new AsyncTaskService(this, UrlService.SEARCH_DRIVER, ResponseCode.SEARCHDRIVER, MODE.POST,
								mStringBuilder.toString(), true).execute();
					} catch (Exception e) {
						e.printStackTrace();
					}
				} else {
					mMainControllerApplication.showError(getResources().getString(R.string.error_internet),
							R.drawable.repeating_bg_error);
				}

			} else if (resultCode == RESULT_CANCELED) {
				mMainControllerApplication.showError(getResources().getString(R.string.error_scan),
						R.drawable.repeating_bg_error);
			}
			break;

		}

	}

	@Override
	public void onServiceResponse(StringBuilder result, int responseCode) {
		try {
			JSONObject resultJson = new JSONObject(result.toString());
			if (responseCode == ResponseCode.SEARCHDRIVER) {

				mIntent = new Intent(getApplicationContext(), DriverTripDetailActivity.class);
				mIntent.putExtra("Json", resultJson.toString());
				startActivity(mIntent);
				finish();

			} else if (responseCode == ResponseCode.UPLOADIMAGE) {

				imagePath.add(resultJson.getString("ImagePath"));

			} else if (responseCode == ResponseCode.UPLOADDOCUMENT) {
				((MainControllerApplication) getApplication()).showError("Successfully Uploded",
						R.drawable.repeating_bg_successful);
			} else {
				((MainControllerApplication) getApplication())
						.showError(getResources().getString(R.string.error_no_data), R.drawable.repeating_bg_error);
			}
		} catch (Exception e) {
			((MainControllerApplication) getApplication()).showError(getResources().getString(R.string.error_service),
					R.drawable.repeating_bg_error);
		}
	}

	@Override
	public void onServiceError() {
		mMainControllerApplication.showError(getResources().getString(R.string.error_service),
				R.drawable.repeating_bg_error);
	}

	@Override
	public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
		if (mSPLF.getInt("ID", 0) == 1) {
			onRefresh();
		} else {
			mSPLF.setInt("ID", 1);
		}
	}

	@Override
	public void onNothingSelected(AdapterView<?> parent) {

	}

	private void onRefresh() {
		mSPLF.setInt("ID", 0);
		onCreate(msavedInstanceState);
	}

	private void sendDropPointsToServer(final ArrayList<DropoffPoints> listOfDropPoints, final Dialog dialog) {
		JSONArray jsonArray = new JSONArray();
		for (int i = 0; i < listOfDropPoints.size(); i++) {
			JSONObject object = new JSONObject();
			DropoffPoints dropoffPoints = listOfDropPoints.get(i);
			try {
				object.put("dropBookingId", dropoffPoints.dropBookingId);
				object.put("dropLocation", dropoffPoints.dropLocation);
				object.put("dropedBoxes", dropoffPoints.dropedBoxes);
				jsonArray.put(object);

			} catch (JSONException e) {

				e.printStackTrace();
			}
		}

		final RequestQueue queue = Volley.newRequestQueue(this);
		String url = UrlService.BASE_URL + UrlService.ADD_DROP_POINTS;
		LoginActivity.showLog("" + url);
		JsonArrayRequest jsObjRequest = new JsonArrayRequest(Request.Method.POST, url, jsonArray,
				new Response.Listener<JSONArray>() {

					@Override
					public void onResponse(JSONArray response) {
						LoginActivity.showLog("" + response);
						dialog.dismiss();

					}
				}, new Response.ErrorListener() {

					@Override
					public void onErrorResponse(VolleyError error) {
						dialog.dismiss();
						LoginActivity.showLog("" + error);

					}
				});

		queue.add(jsObjRequest);

	}

	private class UploadToS3Bucket extends AsyncTask<String, Void, UploadImage> {
		@Override
		protected UploadImage doInBackground(String... urls) {
			String ACCESS_KEY = "AKIAIJALK2XEKW3HG4EA", SECRET_KEY = "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",
					MY_BUCKET = "truxs3/client";// ,//OBJECT_KEY = "unique_id";
			AWSCredentials credentials = new BasicAWSCredentials(ACCESS_KEY, SECRET_KEY);
			AmazonS3 s3 = new AmazonS3Client(credentials);
			java.security.Security.setProperty("networkaddress.cache.ttl", "60");
			s3.setEndpoint("https://s3-ap-southeast-1.amazonaws.com/");
			TransferUtility transferUtility = new TransferUtility(s3, getApplicationContext());
			File UPLOADING_IMAGE = new File(urls[0]);
			String s3bucketurl = "https://s3-ap-southeast-1.amazonaws.com/truxs3/client/" + UPLOADING_IMAGE.getName();
			LoginActivity.showLog("s3bucketurl " + s3bucketurl);
			UploadImage uploadImage = new UploadImage();
			TransferObserver observer = transferUtility.upload(MY_BUCKET, UPLOADING_IMAGE.getName(), UPLOADING_IMAGE);
			uploadImage.observer = observer;
			uploadImage.imageUrl = s3bucketurl;
			uploadImage.UPLOADING_IMAGE = UPLOADING_IMAGE;
			return uploadImage;
		}

		@Override
		protected void onPostExecute(UploadImage uploadImage) {
			LoginActivity.showLog("observer   " + uploadImage.observer);
			mBitmapforImages[mIndexforImages] = BitmapFactory.decodeFile(_path);
			imagePath.add(mIndexforImages, uploadImage.imageUrl);
			mImageViewforImages[mIndexforImages].setImageBitmap(mBitmapforImages[mIndexforImages]);
			uploadImage.observer.setTransferListener(new TransferListener() {
				@Override
				public void onStateChanged(int id, TransferState state) {
					LoginActivity.showLog(state.name() + "URL " + state.ordinal() + "" + id);
				}

				@Override
				public void onProgressChanged(int id, long bytesCurrent, long bytesTotal) {
					int percentage = (int) (bytesCurrent / bytesTotal * 100);
					LoginActivity.showLog("percentage" + percentage);
				}

				@Override
				public void onError(int id, Exception ex) {
					Log.e("Error  ", "" + ex);
				}
			});
		}
	}
}
