/*package com.trux.app.truxfield;

import java.io.IOException;
import java.net.URI;
import java.net.URL;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.ImageLoader;
import com.android.volley.toolbox.Volley;
import com.trux.app.truxfield.services.ServiceHandler;
import com.trux.truxagent.utils.LruBitmapCache;

import android.app.Application;
import android.content.DialogInterface;
import android.content.DialogInterface.OnClickListener;
import android.graphics.Bitmap;
import android.location.Location;
import android.os.AsyncTask;
import android.preference.PreferenceManager;
import android.provider.SyncStateContract.Constants;
import android.text.TextUtils;

*//**
 * Created by Andrea Tortorella on 24/01/14.
 *//*
// @ReportsCrashes(formKey = "", formUri =
// "https://bowstringstudio.cloudant.com/acra-nearllycustomer/_design/acra-storage/_update/report",
// formUriBasicAuthLogin = "arequeseturestreastandan", formUriBasicAuthPassword
// = "AfvwSXcElBD1UvDmalODdBjf", httpMethod = HttpSender.Method.PUT, reportType
// = HttpSender.Type.JSON)
public class MyApplication extends Application {

	private static MyApplication self;
	private AgentProfile currentAgent;
	// private AgentProfile agentId;

	Bitmap visitingCardImage;

	ServiceHandler serviceHandler;

	private int requestCount = 0;
	protected static final String SENDER_ID = "978869952860";
	private RequestQueue mRequestQueue;
	private ImageLoader mImageLoader;
	public Location currentLocation;

	public List<OnClickListener> getClickListenerQueue() {
		return clickListenerQueue;
	}

	private List<DialogInterface.OnClickListener> clickListenerQueue = new ArrayList<DialogInterface.OnClickListener>();
	List<VehicleType> vehicleList = new ArrayList<VehicleType>();
	List<OrgNames> orgNamesList = new ArrayList<OrgNames>();
	List<DriverRegistration> driverNameList = new ArrayList<DriverRegistration>();

	List<TestEntity> testEntities = new ArrayList<TestEntity>();

	List<TestEntity2> testEntities2 = new ArrayList<TestEntity2>();

	List<AccessRightEnum> accessRightList = new ArrayList<AccessRightEnum>();

	String id;

	@Override
	public void onCreate() {
		super.onCreate();
		// ACRA.init(this);
		self = this;

		currentAgent = new AgentProfile();
		id = currentAgent.getId();
	}

	public static final MyApplication app() {
		return self;
	}

	public static synchronized MyApplication getInstance() {
		return self;
	}

	public RequestQueue getRequestQueue() {
		if (mRequestQueue == null) {
			mRequestQueue = Volley.newRequestQueue(getApplicationContext());
		}

		return mRequestQueue;
	}

	public ImageLoader getImageLoader() {
		getRequestQueue();
		if (mImageLoader == null) {
			mImageLoader = new ImageLoader(this.mRequestQueue, new LruBitmapCache());
		}
		return this.mImageLoader;
	}

	public <T> void addToRequestQueue(Request<T> req, String tag) {
		// set the default tag if tag is empty
		req.setTag(TextUtils.isEmpty(tag) ? Constants.TAG : tag);
		getRequestQueue().add(req);
	}

	public <T> void addToRequestQueue(Request<T> req) {
		req.setTag(Constants.TAG);
		getRequestQueue().add(req);
		requestCount++;
	}

	public void requestComplete() {
		requestCount--;
	}

	public int getRequestCount() {
		return requestCount;
	}

	public void cancelPendingRequests(Object tag) {
		if (mRequestQueue != null) {
			mRequestQueue.cancelAll(tag);
		}
	}

	
	 * public void setBitmapforVehicleReg(Bitmap[] bitmap, final
	 * ModelToViewDeliveryListener listener) {
	 * 
	 * Handler handler = new Handler(); final String path = " "; final
	 * ResponseTypeEnum responseType = ResponseTypeEnum.Image_Upload;
	 * 
	 * handler.postDelayed(new Runnable() {
	 * 
	 * @Override public void run() { listener.sendToView(path, responseType); }
	 * }, 10000);
	 * 
	 * }
	 * 
	 * public void setBitmapforDriverReg(Bitmap bitmap, final
	 * ModelToViewDeliveryListener listener) {
	 * 
	 * Handler handler = new Handler(); final String path = " "; final
	 * ResponseTypeEnum responseType = ResponseTypeEnum.Image_Upload;
	 * 
	 * handler.postDelayed(new Runnable() {
	 * 
	 * @Override public void run() { listener.sendToView(path, responseType); }
	 * }, 10000); }
	 

	
	 * public void getLocation(final double latitude, final double longitude,
	 * String mMobileNumber, final ModelToViewDeliveryListener listener) { final
	 * ResponseTypeEnum responseType = ResponseTypeEnum.Location; String url =
	 * getString(R.string.base_url) + "Location/" + latitude + "/" + longitude +
	 * "/" + mMobileNumber; try { URL test = new URL(url); URI uri = new
	 * URI(test.getProtocol(), test.getUserInfo(), test.getHost(),
	 * test.getPort(), test.getPath(), test.getQuery(), test.getRef()); url =
	 * uri.toURL().toString(); } catch (Exception e) { e.printStackTrace(); } }
	 

	public void login(final String email, final String password, final ModelToViewDeliveryListener listener) {

		new AsyncTask<Void, Void, String>() {

			@Override
			protected String doInBackground(Void... arg0) {
				GoogleCloudMessaging gcm = null;
				String regid = null;

				try {
					if (gcm == null) {
						while (gcm == null)
							gcm = GoogleCloudMessaging.getInstance(getApplicationContext());

						while (regid == null) {
							try {
								regid = gcm.register(SENDER_ID);
							} catch (IOException e) {
								continue;
							}
						}
					}
				} catch (Exception e) {

					e.printStackTrace();
				}
				return regid;
			}

			@Override
			protected void onPostExecute(String result) {
				super.onPostExecute(result);
				final ResponseTypeEnum responseType = ResponseTypeEnum.Login;

				Map<String, String> params = new HashMap<String, String>();
				params.put("username", email);
				params.put("password", password);
				params.put("gcmId", "987654321");

				
				 * params.put( "Lat", currentLocation != null ? String
				 * .valueOf(currentLocation.getLatitude()) : "0"); params.put(
				 * "Lng", currentLocation != null ? String
				 * .valueOf(currentLocation.getLongitude()) : "0");
				 
				// params.put("Ip", getLocalIpAddress());

				String url = getString(R.string.base_url) + "loginAgent";

				LoginJsonRequest request = new LoginJsonRequest(Request.Method.POST, url, params,
						new Response.Listener<AgentProfile>() {

					@Override
					public void onResponse(AgentProfile response) {
						// String ID = agentId.getId().toString();
						PreferenceManager.getDefaultSharedPreferences(getApplicationContext()).edit()
								.putBoolean(Constants.IS_LOGIN, true).putString("id", response.getId())
								.putString("email", response.getEmail()).putString("firstName", response.getFirstName())
								.putString("lastName", response.getLastName()).putString("gcmId", response.getGcmId())
								.putString("phone", response.getPhone()).commit();
						currentAgent = response;
						listener.sendToView(response, responseType);
					}
				}, new Response.ErrorListener() {

					@Override
					public void onErrorResponse(VolleyError error) {
						listener.sendToView(error, responseType);
					}
				});

				addToRequestQueue(request);
			}
		}.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, null, null, null);

	}

	 Function to get Access Rights for the Agent 

	public void getAccessRights(String id, final ModelToViewDeliveryListener listener) {

		final ResponseTypeEnum responseType = ResponseTypeEnum.Access_Rights;
		String url = getString(R.string.base_url) + "moduleAccessRights?agentId=" + id;

		try {
			URL test = new URL(url);
			URI uri = new URI(test.getProtocol(), test.getUserInfo(), test.getHost(), test.getPort(), test.getPath(),
					test.getQuery(), test.getRef());
			url = uri.toURL().toString();
		} catch (Exception e) {
			e.printStackTrace();
		}

		RightAccessJsonRequest request = new RightAccessJsonRequest(Request.Method.GET, url,
				new Response.Listener<List<AccessRightEnum>>() {

					@Override
					public void onResponse(List<AccessRightEnum> response) {
						listener.sendToView(response, responseType);
					}
				}, new Response.ErrorListener() {

					@Override
					public void onErrorResponse(VolleyError error) {
						listener.sendToView(error, responseType);
					}
				});
		addToRequestQueue(request);

	}

	 Function to create Lead 

	public void LeadGeneration(LeadGenerationProfile leadGenerationProfile,
			final ModelToViewDeliveryListener listener) {

		final ResponseTypeEnum responseType = ResponseTypeEnum.Lead_Generation;

		Map<String, String> params = new HashMap<String, String>();

		params.put("imageOfVisitingCard",
				leadGenerationProfile.getImagePath() == null || leadGenerationProfile.getImagePath().isEmpty() ? "0"
						: leadGenerationProfile.getImagePath());
		params.put("comments", leadGenerationProfile.getNotes() == null || leadGenerationProfile.getNotes().isEmpty()
				? "0" : leadGenerationProfile.getNotes());
		params.put("scheMeetingDate", leadGenerationProfile.getDate() == null ? "0" : leadGenerationProfile.getDate());
		params.put("scheMeetingTime", leadGenerationProfile.getTime() == null ? "0" : leadGenerationProfile.getTime());
		params.put("scheMeetingTitle",
				leadGenerationProfile.getMeetingTitle() == null || leadGenerationProfile.getMeetingTitle().isEmpty()
						? "0" : leadGenerationProfile.getMeetingTitle());
		params.put("sendProposal", leadGenerationProfile.isShouldSendProposal() ? "1" : "0");
		params.put("agentId",
				currentAgent.getId() == null || currentAgent.getId().isEmpty() ? "0" : currentAgent.getId());

		String url = getString(R.string.base_url) + "visitingCardUpload";
		try {
			URL test = new URL(url);
			URI uri = new URI(test.getProtocol(), test.getUserInfo(), test.getHost(), test.getPort(), test.getPath(),
					test.getQuery(), test.getRef());
			url = uri.toURL().toString();
		} catch (Exception e) {
			e.printStackTrace();
		}

		LeadGenerationJsonRequest request = new LeadGenerationJsonRequest(Request.Method.POST, url, params,
				new Response.Listener<LeadGenerationProfile>() {

					@Override
					public void onResponse(LeadGenerationProfile response) {
						listener.sendToView(response, responseType);
					}
				}, new Response.ErrorListener() {

					@Override
					public void onErrorResponse(VolleyError error) {
						listener.sendToView(error, responseType);
					}
				});
		addToRequestQueue(request);
	}

	
	 * Function to Register Vehicle
	 

	public void setVehicleRegistration(VehicleEntity vehicleEntity, final ModelToViewDeliveryListener listener) {
		new VehicleRegistrationRequest(getString(R.string.base_url), vehicleEntity, listener);
	}

	 function to get Vehicle List 

	public void getVehicleType(final ModelToViewDeliveryListener listener) {

		final ResponseTypeEnum responseType = ResponseTypeEnum.Vehicle_List;

		 http://truxapp.com/trux/lgapi/ 
		String url = getString(R.string.base_url) + "vehicleList";

		try {
			URL test = new URL(url);
			URI uri = new URI(test.getProtocol(), test.getUserInfo(), test.getHost(), test.getPort(), test.getPath(),
					test.getQuery(), test.getRef());
			url = uri.toURL().toString();
		} catch (Exception e) {
			e.printStackTrace();
		}

		VehicleListJsonRequest request = new VehicleListJsonRequest(Request.Method.GET, url,
				new Response.Listener<List<VehicleType>>() {

					@Override
					public void onResponse(List<VehicleType> response) {
						// vehicleList = response;
						listener.sendToView(response, responseType);
					}
				}, new Response.ErrorListener() {

					@Override
					public void onErrorResponse(VolleyError error) {
						listener.sendToView(error, responseType);
					}
				});
		addToRequestQueue(request);

	}

	
	 * Function to Register Driver
	 

	public void setDriverRegistration(DriverProfile driverReg, final ModelToViewDeliveryListener listener) {
		new DriverRegistrationRequest(getString(R.string.base_url), driverReg, listener);
	}

	 Function for Collection Amount 

	public void setCollectionAmount(CollectionAmount collectionAmount, final ModelToViewDeliveryListener listener) {

		final ResponseTypeEnum responseType = ResponseTypeEnum.Collection_Amount;
		Map<String, String> params = new HashMap<String, String>();
		params.put("driverMobile",
				collectionAmount.getMobileNumber() == null || collectionAmount.getMobileNumber().isEmpty() ? "0"
						: collectionAmount.getMobileNumber());
		params.put("depositAmount",
				collectionAmount.getDepositAmount() == null || collectionAmount.getDepositAmount().isEmpty() ? "0"
						: collectionAmount.getDepositAmount());

		String url = getString(R.string.base_url) + "driverCollectionAmount";
		try {
			URL test = new URL(url);
			URI uri = new URI(test.getProtocol(), test.getUserInfo(), test.getHost(), test.getPort(), test.getPath(),
					test.getQuery(), test.getRef());
			url = uri.toURL().toString();
		} catch (Exception e) {
			e.printStackTrace();
		}
		CollectionAmtJsonRequest request = new CollectionAmtJsonRequest(Request.Method.POST, url, params,
				new Response.Listener<Boolean>() {

					@Override
					public void onResponse(Boolean response) {
						listener.sendToView(response, responseType);
					}
				}, new Response.ErrorListener() {

					@Override
					public void onErrorResponse(VolleyError error) {
						listener.sendToView(error, responseType);
					}
				});
		addToRequestQueue(request);
	}

	 FOR DRIVER LIST 

	public void getDriverList(String organizationId, final ModelToViewDeliveryListener listener) {

		final ResponseTypeEnum responseType = ResponseTypeEnum.Driver_List;
		Map<String, String> params = new HashMap<String, String>();
		if (organizationId != null) {
			params.put("organizationId", organizationId);
		}
		// String url =
		// "http://192.168.0.39:8080/trux/attandance/getDriverDetailsList" +
		// "?organizationId=" + organizationId;

		String url = "http://52.77.239.3:8080/trux/attandance/getDriverDetailsList " + "?organizationId="
				+ organizationId;
		
		 * String url =
		 * "http://staging.truxapp.com/trux/attandance/getDriverDetailsList " +
		 * "?organizationId=" + organizationId;
		 
		try {
			URL test = new URL(url);
			URI uri = new URI(test.getProtocol(), test.getUserInfo(), test.getHost(), test.getPort(), test.getPath(),
					test.getQuery(), test.getRef());
			url = uri.toURL().toString();

			DriverNamesJsonRequest request = new DriverNamesJsonRequest(Request.Method.GET, url,
					new com.android.volley.Response.Listener<List<DriverRegistration>>() {

						@Override
						public void onResponse(List<DriverRegistration> response) {
							// response
							// OrgNames orgNames = new OrgNames();
							// orgNames.getName();
							for (DriverRegistration dto : response) {
								driverNameList.add(dto);
							}

							listener.sendToView(response, responseType);

						}
					},
					// new Response.Listener<List<DriverRegistration>>() {
					//
					// @Override
					// public void onResponse(List<DriverRegistration> response)
					// {
					// listener.sendToView(response, responseType);
					// }
					// },
					new com.android.volley.Response.ErrorListener() {

						@Override
						public void onErrorResponse(VolleyError error) {

							listener.sendToView(error, responseType);
						}
					});
			addToRequestQueue(request);
		} catch (Exception e) {
			e.printStackTrace();
		}

	}

	 FOR TEST 

	public void getTest(final ModelToViewDeliveryListener listener) {

		// final ResponseTypeEnum responseType =
		// ResponseTypeEnum.Driver_Attendance;

		 TO BE CONSIDERED 

		final ResponseTypeEnum responseType = ResponseTypeEnum.Test; // for
																		// organisation

		// String url =
		// "http://192.168.0.39:8080/trux/attandance/organizationList";

		String url = "http:/52.77.239.3:8080/trux/subapi/subclient?userId=37";
		
		 * String url =
		 * "http://staging.truxapp.com/trux/subapi/subclient?userId=37";
		 

		try {
			URL test = new URL(url);
			URI uri = new URI(test.getProtocol(), test.getUserInfo(), test.getHost(), test.getPort(), test.getPath(),
					test.getQuery(), test.getRef());
			url = uri.toURL().toString();
		} catch (Exception e) {
			e.printStackTrace();
		}

		TestJsonRequest request = new TestJsonRequest(Request.Method.GET, url,
				new com.android.volley.Response.Listener<List<TestEntity>>() {

					@Override
					public void onResponse(List<TestEntity> response) {
						// response
						// OrgNames orgNames = new OrgNames();
						// orgNames.getName();
						for (TestEntity dto : response) {
							testEntities.add(dto);
						}

						listener.sendToView(response, responseType);

					}
				}, new com.android.volley.Response.ErrorListener() {

					@Override
					public void onErrorResponse(VolleyError error) {

						listener.sendToView(error, responseType);
					}
				});
		addToRequestQueue(request);

	}

	 FOR TEST2 

	public void getTest2(String idClientSubMaster, final ModelToViewDeliveryListener listener) {

		final ResponseTypeEnum responseType = ResponseTypeEnum.Driver_List;
		Map<String, String> params = new HashMap<String, String>();
		if (idClientSubMaster != null) {
			params.put("idClientSubMaster", idClientSubMaster);
		}
		// String url =
		// "http://192.168.0.39:8080/trux/attandance/getDriverDetailsList" +
		// "?organizationId=" + organizationId;
		String url = "http://52.77.239.3:8080/trux/subapi/driverMobileVehicleList " + "?subClientId="
				+ idClientSubMaster;
		
		 * String url =
		 * "http://staging.truxapp.com/trux/subapi/driverMobileVehicleList " +
		 * "?subClientId=" + idClientSubMaster;
		 
		try {
			URL test = new URL(url);
			URI uri = new URI(test.getProtocol(), test.getUserInfo(), test.getHost(), test.getPort(), test.getPath(),
					test.getQuery(), test.getRef());
			url = uri.toURL().toString();

			Test2JsonRequest request = new Test2JsonRequest(Request.Method.GET, url,
					new com.android.volley.Response.Listener<List<TestEntity2>>() {

						@Override
						public void onResponse(List<TestEntity2> response) {
							// response
							// OrgNames orgNames = new OrgNames();
							// orgNames.getName();
							for (TestEntity2 dto : response) {
								testEntities2.add(dto);
							}

							listener.sendToView(response, responseType);

						}
					},
					// new Response.Listener<List<DriverRegistration>>() {
					//
					// @Override
					// public void onResponse(List<DriverRegistration> response)
					// {
					// listener.sendToView(response, responseType);
					// }
					// },
					new com.android.volley.Response.ErrorListener() {

						@Override
						public void onErrorResponse(VolleyError error) {

							listener.sendToView(error, responseType);
						}
					});
			addToRequestQueue(request);
		} catch (Exception e) {
			e.printStackTrace();
		}

	}

	public void getOrgList1(final ModelToViewDeliveryListener listener) {

		// final ResponseTypeEnum responseType =
		// ResponseTypeEnum.Driver_Attendance;

		 TO BE CONSIDERED 

		final ResponseTypeEnum responseType = ResponseTypeEnum.Attendance_Leased_Org; // for
																						// organisation

		// String url =
		// "http://192.168.0.39:8080/trux/attandance/organizationList";

		String url = "http://52.77.239.3:8080/trux/attandance/organizationList";
		
		 * String url =
		 * "http://staging.truxapp.com/trux/attandance/organizationList";
		 

		try {
			URL test = new URL(url);
			URI uri = new URI(test.getProtocol(), test.getUserInfo(), test.getHost(), test.getPort(), test.getPath(),
					test.getQuery(), test.getRef());
			url = uri.toURL().toString();
		} catch (Exception e) {
			e.printStackTrace();
		}

		OrgNamesListJsonRequest request = new OrgNamesListJsonRequest(Request.Method.GET, url,
				new com.android.volley.Response.Listener<List<OrgNames>>() {

					@Override
					public void onResponse(List<OrgNames> response) {
						// response
						// OrgNames orgNames = new OrgNames();
						// orgNames.getName();
						for (OrgNames dto : response) {
							orgNamesList.add(dto);
						}

						listener.sendToView(response, responseType);

					}
				}, new com.android.volley.Response.ErrorListener() {

					@Override
					public void onErrorResponse(VolleyError error) {

						listener.sendToView(error, responseType);
					}
				});
		addToRequestQueue(request);

	}

	 FOR DRIVER ATTENDANCE 

	public void getAttendance(AttendanceProfile attendanceProfile, final ModelToViewDeliveryListener listener) {

		final ResponseTypeEnum responseType = ResponseTypeEnum.Attendance;

		int companyId = attendanceProfile.getCompanyId();
		int driverId = attendanceProfile.getDriverId();
		String attendanceDate = attendanceProfile.getAttendanceDate();
		String punchIn = attendanceProfile.getPunchIn();
		String punchOut = attendanceProfile.getPunchOut();
		String openingKilometer = attendanceProfile.getOpeningKilometer();
		String closingKilometer = attendanceProfile.getClosingKilometer();
		String tolltax = attendanceProfile.getTolltax();
		int createdBy = attendanceProfile.getCreatedBy();

		String url = "http://52.77.239.3:8080/trux/attandance/attandanceDrivers?driverName=" + driverId + "&orgName="
				+ companyId + "&attandanceDate=" + attendanceDate + "&checkin=" + punchIn + "&checkout=" + punchOut
				+ "&openingkm=" + openingKilometer + "&closingkm=" + closingKilometer + "&tolltax=" + tolltax
				+ "&userId=" + createdBy;
				
				 * String url =
				 * "http://staging.truxapp.com/trux/attandance/attandanceDrivers?driverName="
				 * +driverId+"&orgName="+companyId+"&attandanceDate="+
				 * attendanceDate+"&checkin="+punchIn+"&checkout="+punchOut+
				 * "&openingkm="+openingKilometer+"&closingkm="+closingKilometer
				 * +"&tolltax="+tolltax+"&userId="+createdBy;
				 

		
		 * new AsyncTask<Void, Void, String>() {
		 * 
		 * @Override protected String doInBackground(Void... arg0) {
		 * GoogleCloudMessaging gcm = null; String regid = null;
		 * 
		 * try { if (gcm == null) { while (gcm == null) gcm =
		 * GoogleCloudMessaging .getInstance(getApplicationContext());
		 * 
		 * while (regid == null) { try { regid = gcm.register(SENDER_ID); }
		 * catch (IOException e) { continue; } } } } catch (Exception e) {
		 * 
		 * e.printStackTrace(); } return regid; }
		 * 
		 * @Override protected void onPostExecute(String result) {
		 * super.onPostExecute(result); final ResponseTypeEnum responseType =
		 * ResponseTypeEnum.Driver_Attendance; AttendanceProfile ap = new
		 * AttendanceProfile(); Map params = new HashMap(); params.put("userId",
		 * uId); params.put("driverName", driverIds); params.put("orgName",
		 * companyIds); params.put("attandanceDate", atDate);
		 * params.put("checkin", punchIn); params.put("checkout", punchOut);
		 * params.put("openingkm", openKm); params.put("closingkm", closeKm);
		 * params.put("tolltax", tollTax); //String url =
		 * "http://192.168.0.39:8080/trux/attandance/attandanceDrivers"; String
		 * url = "http://staging.truxapp.com/trux/attandance/attandanceDrivers";
		 * AttendanceJsonRequest request = new
		 * AttendanceJsonRequest(Request.Method.POST, url, params, new
		 * Response.Listener < List < AttendanceProfile >> () {
		 * 
		 * //@Override public void onResponse(List<AttendanceProfile> response)
		 * { //String ID = agentId.getId().toString(); // PreferenceManager //
		 * .getDefaultSharedPreferences( // getApplicationContext()).edit() //
		 * .putInt("driverName", response.getDriverId()) // .putInt("orgName",
		 * response.getCompanyId()) // .putString("attendanceDate",
		 * response.getAttendanceDate()) // .putString("checkin",
		 * response.getPunchIn()) // .putString("checkout",
		 * response.getPunchOut()) // .putString("openingkm",
		 * response.getOpeningKilometer()) // .putString("closingkm",
		 * response.getClosingKilometer()) // .putString("tolltax",
		 * response.getTolltax()) // .putString("userId", response.getId()) //
		 * .commit(); listener.sendToView(response, responseType); } }, new
		 * Response.ErrorListener() {
		 * 
		 * @Override public void onErrorResponse(VolleyError error) {
		 * listener.sendToView(error, responseType); } });
		 * 
		 * 
		 * addToRequestQueue(request); }
		 * }.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, null, null,
		 * null);
		 * 
		 

		try {
			URL test = new URL(url);
			URI uri = new URI(test.getProtocol(), test.getUserInfo(), test.getHost(), test.getPort(), test.getPath(),
					test.getQuery(), test.getRef());
			url = uri.toURL().toString();
		} catch (Exception e) {
			e.printStackTrace();
		}
		AttendanceJsonRequest request = new AttendanceJsonRequest(Request.Method.GET, url, attendanceProfile,
				new Response.Listener<Boolean>() {

					@Override
					public void onResponse(Boolean response) {
						listener.sendToView(response, responseType);
					}
				}, new Response.ErrorListener() {

					@Override
					public void onErrorResponse(VolleyError error) {
						listener.sendToView(error, responseType);
					}
				});
		addToRequestQueue(request);
	}

	 Function for Driver's Part Payment 

	public void setDriverPartPayment(PartPayment partPayment, final ModelToViewDeliveryListener listener) {

		final ResponseTypeEnum responseType = ResponseTypeEnum.Driver_Part_Payment;

		String amount_paid = partPayment.getAmountPaid();
		String vehicle_number = partPayment.getVehicleNumber();
		String comment = partPayment.getComment();
		String paid_by = id;
		String driver_mobile = partPayment.getDriverMobile();

		System.out.println(currentAgent.getId());

		// paid_by = id;
		
		 * TO ADD IN JSON OBJECT
		 * 
		 * //TO ADD DETAILS JSONObject params = new JSONObject();
		 * 
		 * try { params.put("driverMobile", partPayment.getDriverMobile() ==
		 * null || partPayment.getDriverMobile().isEmpty() ? "0" :
		 * partPayment.getDriverMobile()); params.put("amountPaid",
		 * partPayment.getAmountPaid() == null ||
		 * partPayment.getAmountPaid().isEmpty() ? "0" :
		 * collectionAmount.getDepositAmount()); params.put("vehicleNumber",
		 * partPayment.getVehicleNumber() == null ||
		 * partPayment.getVehicleNumber().isEmpty() ? "0" :
		 * partPayment.getVehicleNumber()); params.put("comment",
		 * partPayment.getComment() == null ||
		 * partPayment.getComment().isEmpty() ? "0" : partPayment.getComment());
		 * params.put("datePaidOn", partPayment.getDatePaidOn() == null ||
		 * partPayment.getDatePaidOn().isEmpty() ? "0" :
		 * partPayment.getDatePaidOn()); params.put("paidBy",
		 * partPayment.getPaidBy() == null || partPayment.getPaidBy().isEmpty()
		 * ? "0" : partPayment.getPaidBy()); } catch (JSONException e) {
		 * e.printStackTrace(); }
		 

		String url = getString(R.string.driver_part_payment) + amount_paid + "/" + vehicle_number + "/" + comment + "/"
				+ paid_by + "/" + driver_mobile;

		
		 * serviceHandler = new ServiceHandler();
		 * 
		 * serviceHandler.executeAsync(url, 1);
		 * 
		 * new AddNewPrediction().execute(url);
		 

		try {
			URL test = new URL(url);
			URI uri = new URI(test.getProtocol(), test.getUserInfo(), test.getHost(), test.getPort(), test.getPath(),
					test.getQuery(), test.getRef());
			url = uri.toURL().toString();
		} catch (Exception e) {
			e.printStackTrace();
		}
		DriverPartPaymentJsonRequest request = new DriverPartPaymentJsonRequest(Request.Method.GET, url, partPayment,
				new Response.Listener<Boolean>() {

					@Override
					public void onResponse(Boolean response) {
						listener.sendToView(response, responseType);
					}
				}, new Response.ErrorListener() {

					@Override
					public void onErrorResponse(VolleyError error) {
						listener.sendToView(error, responseType);
					}
				});
		addToRequestQueue(request);

	}

	 Function for Driver Deactivation 

	public void getDriverDeactivate(String id, final ModelToViewDeliveryListener listener) {

		final ResponseTypeEnum responseType = ResponseTypeEnum.Driver_Deactivate;
		String url = getString(R.string.base_url) + "driverDeactivate?driverMobile=" + id;

		try {
			URL test = new URL(url);
			URI uri = new URI(test.getProtocol(), test.getUserInfo(), test.getHost(), test.getPort(), test.getPath(),
					test.getQuery(), test.getRef());
			url = uri.toURL().toString();
		} catch (Exception e) {
			e.printStackTrace();
		}
		DriverDeactivateJsonRequest request = new DriverDeactivateJsonRequest(Request.Method.GET, url,
				new Response.Listener<Boolean>() {

					@Override
					public void onResponse(Boolean response) {
						listener.sendToView(response, responseType);
					}
				}, new Response.ErrorListener() {

					@Override
					public void onErrorResponse(VolleyError error) {
						listener.sendToView(error, responseType);
					}
				});
		addToRequestQueue(request);

	}

	 driver Velidation get method 

	
	 * public void getDriverVelidate(String driverPhone, String deviceId, final
	 * ModelToViewDeliveryListener listener) {
	 * 
	 * final ResponseTypeEnum responseType = ResponseTypeEnum.Driver_Velidate;
	 * String url = getString(R.string.base_url) +
	 * "velidateDriver?driverPhoneNumber=" + "9311634889" + "&deviceId" +
	 * "Nexus";
	 * 
	 * try { URL test = new URL(url); URI uri = new URI(test.getProtocol(),
	 * test.getUserInfo(), test.getHost(), test.getPort(), test.getPath(),
	 * test.getQuery(), test.getRef()); url = uri.toURL().toString(); } catch
	 * (Exception e) { e.printStackTrace(); } DriverVelidateJsonRequest request
	 * = new DriverVelidateJsonRequest( Request.Method.GET, url, new
	 * Response.Listener<DriverVelidate>() {
	 * 
	 * @Override public void onResponse(DriverVelidate response) {
	 * listener.sendToView(response, responseType); } }, new
	 * Response.ErrorListener() {
	 * 
	 * @Override public void onErrorResponse(VolleyError error) {
	 * listener.sendToView(error, responseType); } });
	 * addToRequestQueue(request);
	 * 
	 * }
	 
	
	 * Send Reference tp another User Get data from user and send it to Server
	 

	public void Reference(final ModelToViewDeliveryListener listener) {

		final ResponseTypeEnum responseType = ResponseTypeEnum.Reference_Provide;
		Map<String, String> params = new HashMap<String, String>();
		params.put("Name", referenceProfile.getName() == null || referenceProfile.getName().isEmpty() ? "0"
				: referenceProfile.getName());
		params.put("MobileNumber", referenceProfile.getPhone() == null || referenceProfile.getPhone().isEmpty() ? "0"
				: referenceProfile.getPhone());

		String url = getString(R.string.base_url) + "SendReference";
		try {
			URL test = new URL(url);
			URI uri = new URI(test.getProtocol(), test.getUserInfo(), test.getHost(), test.getPort(), test.getPath(),
					test.getQuery(), test.getRef());
			url = uri.toURL().toString();
		} catch (Exception e) {
			e.printStackTrace();
		}
		ReferenceJsonRequest request = new ReferenceJsonRequest(Request.Method.POST, url, params,
				new Response.Listener<ReferenceProfile>() {

					@Override
					public void onResponse(ReferenceProfile response) {
						referenceProfile = response;
						listener.sendToView(response, responseType);
					}
				}, new Response.ErrorListener() {

					@Override
					public void onErrorResponse(VolleyError error) {
						listener.sendToView(error, responseType);
					}
				});
		addToRequestQueue(request);
	}

	 Getter and Setter for Agent Profile 

	public AgentProfile getCurrentAgent() {
		return currentAgent;
	}

	public void setCurrentAgent(AgentProfile currentAgent) {
		this.currentAgent = currentAgent;
	}

	 Getter and setter for Access rights 

	public List<AccessRightEnum> getAccessList() {
		return accessRightList;
	}

	public void setAccessRightList(List<AccessRightEnum> accessRightList) {
		this.accessRightList = accessRightList;
	}

	 Getter and Setter for Vehicle Entity 

	public VehicleEntity getVehicleEntity() {
		return vehicleEntity;
	}

	public void setVehicleEntity(VehicleEntity vehicleEntity) {
		this.vehicleEntity = vehicleEntity;
	}

	 Getter and Setter for Vehicle List 

	public List<VehicleType> getVehicleList() {
		return vehicleList;
	}

	public void setVehicleList(List<VehicleType> vehicleList) {
		this.vehicleList = vehicleList;
	}

	 Getter and Setter for Test 

	public List<TestEntity> getTest() {
		return testEntities;
	}

	public void setTest(List<TestEntity> testEntities) {
		this.testEntities = testEntities;
	}

	 Getter ans Setter for Test2 

	public List<TestEntity2> getTest2() {
		return testEntities2;
	}

	public void setTest2(List<TestEntity2> testEntities2) {
		this.testEntities2 = testEntities2;
	}

	 Getter and Setter for OrgList 

	public List<OrgNames> getOrgList1() {
		return orgNamesList;
	}

	public void setOrgList1(List<OrgNames> orgNamesList) {
		this.orgNamesList = orgNamesList;
	}

	 Getter ans Setter for Driver Name List 

	public List<DriverRegistration> getDriverNameList() {
		return driverNameList;
	}

	public void setDriverNameList(List<DriverRegistration> driverNameList) {
		this.driverNameList = driverNameList;
	}

	 Getter and Setter for Driver Registration 

	public DriverProfile getDriverReg() {
		return driverReg;
	}

	public void setDriverReg(DriverProfile driverReg) {
		this.driverReg = driverReg;
	}

	 Getter and Setter for Lead Generation 

	public LeadGenerationProfile getLeadGeneration() {
		return leadGeneration;
	}

	public void setLeadGeneration(LeadGenerationProfile leadGeneration) {
		this.leadGeneration = leadGeneration;
	}

	 Getter and Setter for Driver's Part Payment 
};*/