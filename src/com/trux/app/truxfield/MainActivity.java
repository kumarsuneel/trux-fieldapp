package com.trux.app.truxfield;

import com.trux.app.truxfield.model.GlobalVariable;
import com.trux.app.truxfield.model.SharedPreferencesField;
import com.trux.app.truxfield.network.NetworkStateReceiver;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences.Editor;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;

public class MainActivity extends AppCompatActivity {
	// implements OnServiceResponseListener
	private SharedPreferencesField mSPLF;
	private Intent mIntent;
	// private StringBuilder mStringBuilder;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);
		getSupportActionBar().hide();
		// mStringBuilder = new StringBuilder();
		mSPLF = new SharedPreferencesField(getApplicationContext());

	}

	public void onCrm(View v) {
		if (!NetworkStateReceiver.isOnline(getApplicationContext())) {
			((MainControllerApplication) getApplication()).showError(getResources().getString(R.string.enable_internet),
					R.drawable.repeating_bg_error);
		} else {
			mIntent = new Intent(MainActivity.this, CRMActivity.class).addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP)
					.addFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP);
			startActivity(mIntent);
		}
	}

	public void onBook(View v) {
		if (!NetworkStateReceiver.isOnline(getApplicationContext())) {
			((MainControllerApplication) getApplication()).showError(getResources().getString(R.string.enable_internet),
					R.drawable.repeating_bg_error);
		} else {
			mIntent = new Intent(MainActivity.this, TripManagement.class).addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP)
					.addFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP);
			startActivity(mIntent);
		}
	}

	public void onLogout(View v) {

		Intent intent = new Intent(this, LoginActivity.class);
		intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP);
		startActivity(intent);
		mSPLF.setBoolean(GlobalVariable.IS_LOGIN, false);
		mSPLF.clear();
		android.os.Process.killProcess(android.os.Process.myPid());
		Editor editor = getSharedPreferences("clear_cache", Context.MODE_PRIVATE).edit();
		editor.clear();
		editor.commit();
		finish();
	}

	/*
	 * @Override public void onServiceResponse(StringBuilder result, int
	 * responseCode) { try { JSONArray resultJson = new
	 * JSONArray(result.substring(result.indexOf("[")));
	 * 
	 * if (responseCode == ResponseCode.SUBCLINT ) {
	 * 
	 * mIntent = new Intent(MainActivity.this, TripManagement.class)
	 * .addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP).addFlags(Intent.
	 * FLAG_ACTIVITY_SINGLE_TOP); // mSPLF.setString("SubclintResponse",
	 * resultJson.toString()); // mIntent.putExtra("resultJson",
	 * mSPLF.getString("SubclintResponse", "")); startActivity(mIntent); //
	 * finish();
	 * 
	 * }else{ ((MainControllerApplication)
	 * getApplication()).showError(getResources().getString(R.string.
	 * error_no_data), R.drawable.repeating_bg_error); } } catch (Exception e) {
	 * ((MainControllerApplication)
	 * getApplication()).showError(getResources().getString(R.string.
	 * error_service), R.drawable.repeating_bg_error); } }
	 * 
	 * @Override public void onServiceError() { // TODO Auto-generated method
	 * stub
	 * 
	 * }
	 */

}
