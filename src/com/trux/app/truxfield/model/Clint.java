package com.trux.app.truxfield.model;

public class Clint {
	public String idClientSubMaster;
	public String idClientMaster;
	public String idClientLevelMaster;
	public String subName;
	public String address;
	public String idCountry;
	public String idZone;
	public String idState;
	public String idCity;
	public String idHub;
	public String isActive;
	public String dateCreated;
	public String createdBy;
	public String dateModified;
	public String modifiedBy;
	public String clientLat;
	public String clientLong;
	public String driverLoginDistance;
	public String errorCode;
	public String errorMessage;
	
	public String getIdClientSubMaster() {
		return idClientSubMaster;
	}
	public void setIdClientSubMaster(String idClientSubMaster) {
		this.idClientSubMaster = idClientSubMaster;
	}
	public String getIdClientMaster() {
		return idClientMaster;
	}
	public void setIdClientMaster(String idClientMaster) {
		this.idClientMaster = idClientMaster;
	}
	public String getIdClientLevelMaster() {
		return idClientLevelMaster;
	}
	public void setIdClientLevelMaster(String idClientLevelMaster) {
		this.idClientLevelMaster = idClientLevelMaster;
	}
	public String getSubName() {
		return subName;
	}
	public void setSubName(String subName) {
		this.subName = subName;
	}
	public String getAddress() {
		return address;
	}
	public void setAddress(String address) {
		this.address = address;
	}
	public String getIdCountry() {
		return idCountry;
	}
	public void setIdCountry(String idCountry) {
		this.idCountry = idCountry;
	}
	public String getIdZone() {
		return idZone;
	}
	public void setIdZone(String idZone) {
		this.idZone = idZone;
	}
	public String getIdState() {
		return idState;
	}
	public void setIdState(String idState) {
		this.idState = idState;
	}
	public String getIdCity() {
		return idCity;
	}
	public void setIdCity(String idCity) {
		this.idCity = idCity;
	}
	public String getIdHub() {
		return idHub;
	}
	public void setIdHub(String idHub) {
		this.idHub = idHub;
	}
	public String getIsActive() {
		return isActive;
	}
	public void setIsActive(String isActive) {
		this.isActive = isActive;
	}
	public String getDateCreated() {
		return dateCreated;
	}
	public void setDateCreated(String dateCreated) {
		this.dateCreated = dateCreated;
	}
	public String getCreatedBy() {
		return createdBy;
	}
	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}
	public String getDateModified() {
		return dateModified;
	}
	public void setDateModified(String dateModified) {
		this.dateModified = dateModified;
	}
	public String getModifiedBy() {
		return modifiedBy;
	}
	public void setModifiedBy(String modifiedBy) {
		this.modifiedBy = modifiedBy;
	}
	public String getClientLat() {
		return clientLat;
	}
	public void setClientLat(String clientLat) {
		this.clientLat = clientLat;
	}
	public String getClientLong() {
		return clientLong;
	}
	public void setClientLong(String clientLong) {
		this.clientLong = clientLong;
	}
	public String getDriverLoginDistance() {
		return driverLoginDistance;
	}
	public void setDriverLoginDistance(String driverLoginDistance) {
		this.driverLoginDistance = driverLoginDistance;
	}
	public String getErrorCode() {
		return errorCode;
	}
	public void setErrorCode(String errorCode) {
		this.errorCode = errorCode;
	}
	public String getErrorMessage() {
		return errorMessage;
	}
	public void setErrorMessage(String errorMessage) {
		this.errorMessage = errorMessage;
	}
	
}
