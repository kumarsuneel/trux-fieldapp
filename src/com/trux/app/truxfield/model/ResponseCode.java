package com.trux.app.truxfield.model;

/**
 * Created by anurag.srivastava.
 */
public class ResponseCode {
	
	public final static int LOGOUT					= 0;
	public final static int LOGIN 					= 1;
	public final static int PUNCHIN					= 2;
	public final static int STARATJOURNEY			= 3;
	public final static int STOPJOURNEY 			= 4;
	public final static int PUNCHOUT				= 5;
	public final static int SUBCLINT 				= 6;
	public final static int DRIVER					= 7;
	public final static int SEARCHDRIVER			= 8;
	public final static int UPLOADDOCUMENT			= 9;
	public final static int UPLOADIMAGE				= 10;
	public final static int VERSIONUPDATER			= 11;
	public final static int VALIDUSER				= 12;

}
