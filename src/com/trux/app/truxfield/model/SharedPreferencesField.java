package com.trux.app.truxfield.model;

import java.util.HashMap;
import java.util.Map;

import android.content.Context;
import android.content.SharedPreferences;

public class SharedPreferencesField {
	private static final String PREF_FILE = "LeaseDriver";
	private SharedPreferences settings;
	private SharedPreferences.Editor editor;
	private static Map<Context, SharedPreferencesField> instances = new HashMap<Context, SharedPreferencesField>();

	public void clear() {
		settings.edit();
		editor.clear();
		editor.commit();
	}

	public SharedPreferencesField(Context context) {
		settings = context.getSharedPreferences(PREF_FILE, Context.MODE_PRIVATE);
		editor = settings.edit();
	}

	public static SharedPreferencesField getInstance(Context context) {
		if (!instances.containsKey(context))
			instances.put(context, new SharedPreferencesField(context));
		return instances.get(context);
	}

	public String getString(String key, String defValue) {
		return settings.getString(key, defValue);
	}

	public SharedPreferencesField setString(String key, String value) {
		editor.putString(key, value);
		editor.commit();
		return this;
	}

	public int getInt(String key, int defValue) {
		return settings.getInt(key, defValue);
	}

	public SharedPreferencesField setInt(String key, int value) {
		editor.putInt(key, value);
		editor.commit();
		return this;
	}

	public boolean getBoolean(String key, boolean defValue) {
		return settings.getBoolean(key, defValue);
	}

	public SharedPreferencesField setBoolean(String key, boolean value) {
		editor.putBoolean(key, value);
		editor.commit();
		return this;
	}
}
