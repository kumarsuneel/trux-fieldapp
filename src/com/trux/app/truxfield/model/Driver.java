package com.trux.app.truxfield.model;

import java.io.Serializable;

public class Driver implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	public String id;
	public String driverId;
	public String vehicleId;
	public String deviceId;
	public String driverPhoneNumber;
	public String deviceUUID;
	public String vehicleNumber;
	public String vehicleType;
	public String driverName;
	public String loginStatus;
	public String driverStatus;
	public String lastLoginTime;
	public String lastLogoutTime;
	public String driver_apk_version;
	public String documentuploadurl;
	public String dstatus;
	public String subClientId;
	public String isValidDriver;
	public String errorCode;
	public String errorMessage;
	public String driverMessage;
	public String driverLoginDate;
	public String driverLoginTime;
	public String driverLogoutDate;
	public String driverLogoutTime;
	public String driverLoginDurationTime;
	public String latestApkVersion;
	public String apkUrl;
	public String driverImage;
	private String bookingLeaseId;

	public String getDriverImage() {
		return driverImage;
	}

	public void setDriverImage(String driverImage) {
		this.driverImage = driverImage;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getDriverId() {
		return driverId;
	}

	public void setDriverId(String driverId) {
		this.driverId = driverId;
	}

	public String getVehicleId() {
		return vehicleId;
	}

	public void setVehicleId(String vehicleId) {
		this.vehicleId = vehicleId;
	}

	public String getDeviceId() {
		return deviceId;
	}

	public void setDeviceId(String deviceId) {
		this.deviceId = deviceId;
	}

	public String getDriverPhoneNumber() {
		return driverPhoneNumber;
	}

	public void setDriverPhoneNumber(String driverPhoneNumber) {
		this.driverPhoneNumber = driverPhoneNumber;
	}

	public String getDeviceUUID() {
		return deviceUUID;
	}

	public void setDeviceUUID(String deviceUUID) {
		this.deviceUUID = deviceUUID;
	}

	public String getVehicleNumber() {
		return vehicleNumber;
	}

	public void setVehicleNumber(String vehicleNumber) {
		this.vehicleNumber = vehicleNumber;
	}

	public String getVehicleType() {
		return vehicleType;
	}

	public void setVehicleType(String vehicleType) {
		this.vehicleType = vehicleType;
	}

	public String getDriverName() {
		return driverName;
	}

	public void setDriverName(String driverName) {
		this.driverName = driverName;
	}

	public String getLoginStatus() {
		return loginStatus;
	}

	public void setLoginStatus(String loginStatus) {
		this.loginStatus = loginStatus;
	}

	public String getDriverStatus() {
		return driverStatus;
	}

	public void setDriverStatus(String driverStatus) {
		this.driverStatus = driverStatus;
	}

	public String getLastLoginTime() {
		return lastLoginTime;
	}

	public void setLastLoginTime(String lastLoginTime) {
		this.lastLoginTime = lastLoginTime;
	}

	public String getLastLogoutTime() {
		return lastLogoutTime;
	}

	public void setLastLogoutTime(String lastLogoutTime) {
		this.lastLogoutTime = lastLogoutTime;
	}

	public String getDriver_apk_version() {
		return driver_apk_version;
	}

	public void setDriver_apk_version(String driver_apk_version) {
		this.driver_apk_version = driver_apk_version;
	}

	public String getDocumentuploadurl() {
		return documentuploadurl;
	}

	public void setDocumentuploadurl(String documentuploadurl) {
		this.documentuploadurl = documentuploadurl;
	}

	public String getDstatus() {
		return dstatus;
	}

	public void setDstatus(String dstatus) {
		this.dstatus = dstatus;
	}

	public String getSubClientId() {
		return subClientId;
	}

	public void setSubClientId(String subClientId) {
		this.subClientId = subClientId;
	}

	public String getIsValidDriver() {
		return isValidDriver;
	}

	public void setIsValidDriver(String isValidDriver) {
		this.isValidDriver = isValidDriver;
	}

	public String getErrorCode() {
		return errorCode;
	}

	public void setErrorCode(String errorCode) {
		this.errorCode = errorCode;
	}

	public String getErrorMessage() {
		return errorMessage;
	}

	public void setErrorMessage(String errorMessage) {
		this.errorMessage = errorMessage;
	}

	public String getDriverMessage() {
		return driverMessage;
	}

	public void setDriverMessage(String driverMessage) {
		this.driverMessage = driverMessage;
	}

	public String getDriverLoginDate() {
		return driverLoginDate;
	}

	public void setDriverLoginDate(String driverLoginDate) {
		this.driverLoginDate = driverLoginDate;
	}

	public String getDriverLoginTime() {
		return driverLoginTime;
	}

	public void setDriverLoginTime(String driverLoginTime) {
		this.driverLoginTime = driverLoginTime;
	}

	public String getDriverLogoutDate() {
		return driverLogoutDate;
	}

	public void setDriverLogoutDate(String driverLogoutDate) {
		this.driverLogoutDate = driverLogoutDate;
	}

	public String getDriverLogoutTime() {
		return driverLogoutTime;
	}

	public void setDriverLogoutTime(String driverLogoutTime) {
		this.driverLogoutTime = driverLogoutTime;
	}

	public String getDriverLoginDurationTime() {
		return driverLoginDurationTime;
	}

	public void setDriverLoginDurationTime(String driverLoginDurationTime) {
		this.driverLoginDurationTime = driverLoginDurationTime;
	}

	public String getLatestApkVersion() {
		return latestApkVersion;
	}

	public void setLatestApkVersion(String latestApkVersion) {
		this.latestApkVersion = latestApkVersion;
	}

	public String getApkUrl() {
		return apkUrl;
	}

	public void setApkUrl(String apkUrl) {
		this.apkUrl = apkUrl;
	}

	public void setBookingLeaseId(String bookingLeaseId) {
		this.bookingLeaseId = bookingLeaseId;

	}

	public String getBookingLeaseId() {
		return bookingLeaseId;
	}

}
