package com.trux.app.truxfield;

import java.util.ArrayList;
import java.util.List;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import com.trux.app.truxfield.interfaces.OnServiceResponseListener;
import com.trux.app.truxfield.model.Clint;
import com.trux.app.truxfield.model.Driver;
import com.trux.app.truxfield.model.GlobalVariable;
import com.trux.app.truxfield.model.ResponseCode;
import com.trux.app.truxfield.model.SharedPreferencesField;
import com.trux.app.truxfield.network.NetworkStateReceiver;
import com.trux.app.truxfield.services.AsyncTaskService;
import com.trux.app.truxfield.services.AsyncTaskService.MODE;
import com.trux.app.truxfield.services.UrlService;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences.Editor;
import android.graphics.Color;
import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.view.KeyEvent;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemSelectedListener;
import android.widget.ArrayAdapter;
import android.widget.Spinner;
import android.widget.TextView;

@SuppressWarnings("deprecation")
public class CreateOrderActivity extends ActionBarActivity implements OnServiceResponseListener {
	
	private ArrayList<Clint> mSubClintList;
	private ArrayList<Driver> mDriverList;
	private ArrayAdapter<String> mSubClintListAdapter,mDriverListAdapter;
	private List<String> msubNameList,mdriverdataList;
	private Driver mDriver;
	private Clint mClint;
	private Intent mIntent;
	private StringBuilder mStringBuilder;
	private Spinner spinnerClient,spinnerDrivre;
	private JSONObject resultsubclint,resultdriver;
	private String resultclint,mSelectDriverPhoneNumber;
	private JSONArray mSubClintjsonArray,mDriverjsonArray;
	private int mSelectClintid;
	private SharedPreferencesField mSPLF;
	@Override
	 public boolean onKeyDown(int keyCode, KeyEvent event) {
	     if ((keyCode == KeyEvent.KEYCODE_BACK)) {
	    	 mIntent = new Intent(CreateOrderActivity.this, MainActivity.class)
				.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP).addFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP);
	    	 startActivity(mIntent);
	    	 finish();
	     }
	     return super.onKeyDown(keyCode, event);
	 }
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_createorder);
        getSupportActionBar().hide();
        mSPLF = new SharedPreferencesField(getApplicationContext());
        mStringBuilder = new StringBuilder();
        mSubClintList = new ArrayList<Clint>();
        mDriverList = new ArrayList<Driver>();
        spinnerClient = (Spinner) findViewById(R.id.spinnerClient);
        spinnerDrivre = (Spinner) findViewById(R.id.spinnerDrivre);
        try {
        	resultclint= mSPLF.getString("SubclintResponse", "");
			mSubClintjsonArray = new JSONArray(resultclint);
			if (mSubClintjsonArray.length() > 0) {
				mSubClintList.clear();
				for (int i =0; i< mSubClintjsonArray.length(); i++) {
					mClint = new Clint();
					try {
						resultsubclint = mSubClintjsonArray.getJSONObject(i);
						mClint.setIdClientSubMaster(resultsubclint.getString("idClientSubMaster").equals("null")||resultsubclint.getString("idClientSubMaster").equals("") ? "N/A" : resultsubclint.getString("idClientSubMaster"));
						mClint.setSubName(resultsubclint.getString("subName").equals("null")||resultsubclint.getString("subName").equals("") ? "N/A" : resultsubclint.getString("subName"));
					} catch (NumberFormatException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					} catch (JSONException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
					
					mSubClintList.add(mClint);
				}
				msubNameList = new ArrayList<String>();
				for (int i =0; i< mSubClintList.size(); i++) {
					msubNameList.add(mSubClintList.get(i).getSubName());
				}
			}
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
       
		mSubClintListAdapter = new ArrayAdapter<String>(this,android.R.layout.simple_spinner_dropdown_item,msubNameList);
        spinnerClient.setAdapter(mSubClintListAdapter);
        spinnerClient.setOnItemSelectedListener(new OnItemSelectedListener() {
    		public void onItemSelected(AdapterView<?> arg0, View arg1,
    				int position, long arg3) {
    			TextView spinnerText = (TextView) spinnerClient.getChildAt(0);
    			spinnerText.setTextColor(Color.WHITE);
    			if (!NetworkStateReceiver.isOnline(getApplicationContext())){
    				((MainControllerApplication) getApplication()).showError(getResources().getString(R.string.enable_internet), R.drawable.repeating_bg_error);
    			}else{
    				mSelectClintid =Integer.parseInt(mSubClintList.get(position).getIdClientSubMaster());
    				try {
						JSONObject jo = new JSONObject();
						jo.put("subClientId", mSelectClintid);
						new AsyncTaskService(CreateOrderActivity.this,UrlService.SUB_CLINT_DRIVER,ResponseCode.DRIVER, MODE.GET,mStringBuilder.toString(),true).execute(jo);
					} catch (JSONException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
    			}
    		}
    		public void onNothingSelected(AdapterView<?> arg0) {
    				
    		}
    	});
    }
    public void onSearchDriver(View v) {
    	if (!NetworkStateReceiver.isOnline(getApplicationContext())){
			((MainControllerApplication) getApplication()).showError(getResources().getString(R.string.enable_internet), R.drawable.repeating_bg_error);
		}else{
			
			mStringBuilder = new StringBuilder();
//			mStringBuilder.append("driver").append("/").append("8860005447");
			mStringBuilder.append("driver").append("/").append(mSelectDriverPhoneNumber);
			new AsyncTaskService(this,UrlService.SEARCH_DRIVER,ResponseCode.SEARCHDRIVER, MODE.POST,mStringBuilder.toString(),true).execute();
		}
    }
 public void onLogout(View v) {
    	
    	Intent intent = new Intent(this, LoginActivity.class);
		intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK| Intent.FLAG_ACTIVITY_CLEAR_TOP);
		startActivity(intent);
		mSPLF.setBoolean(GlobalVariable.IS_LOGIN, false);
		mSPLF.clear();
		android.os.Process.killProcess(android.os.Process.myPid());
	    Editor editor = getSharedPreferences("clear_cache", Context.MODE_PRIVATE).edit();
	    editor.clear();
	    editor.commit();
		finish();
    }
	@Override
	public void onServiceResponse(StringBuilder result, int responseCode) {
		try {
			if (responseCode == ResponseCode.DRIVER ) {
				mDriverjsonArray = new JSONArray(result.substring(result.indexOf("[")));
				if (mDriverjsonArray.length() > 0) {
					mDriverList.clear();
					for (int i =0; i< mDriverjsonArray.length(); i++) {
						mDriver = new Driver();
						try {
							resultdriver = mDriverjsonArray.getJSONObject(i);
							mDriver.setVehicleNumber(resultdriver.getString("vehicleNumber").equals("null")||resultdriver.getString("vehicleNumber").equals("") ? "N/A" : resultdriver.getString("vehicleNumber"));
							mDriver.setDriverName(resultdriver.getString("driverName").equals("null")||resultdriver.getString("driverName").equals("") ? "N/A" : resultdriver.getString("driverName"));
							mDriver.setDriverPhoneNumber(resultdriver.getString("driverPhoneNumber").equals("null")||resultdriver.getString("driverPhoneNumber").equals("") ? "N/A" : resultdriver.getString("driverPhoneNumber"));
						} catch (NumberFormatException e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						} catch (JSONException e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						}
						mDriverList.add(mDriver);
					}
					mdriverdataList = new ArrayList<String>();
					for (int i =0; i< mDriverList.size(); i++) {
						String vehical = mDriverList.get(i).getVehicleNumber();
						String Name = mDriverList.get(i).getDriverName();
						String PhoneNumber = mDriverList.get(i).getDriverPhoneNumber();
						mdriverdataList.add(vehical+","+Name+","+PhoneNumber);
					}
				}	
				mDriverListAdapter = new ArrayAdapter<String>(this,android.R.layout.simple_spinner_dropdown_item,mdriverdataList);
				spinnerDrivre.setAdapter(mDriverListAdapter);
				spinnerDrivre.setOnItemSelectedListener(new OnItemSelectedListener() {
		    		public void onItemSelected(AdapterView<?> arg0, View arg1,int position, long arg3) {
		    			TextView spinnerText = (TextView) spinnerDrivre.getChildAt(0);
		    			spinnerText.setTextColor(Color.WHITE);
		    			mSelectDriverPhoneNumber =mDriverList.get(position).getDriverPhoneNumber();
		    		}
		    		public void onNothingSelected(AdapterView<?> arg0) {
		    				
		    		}
		    	});
			}else if(responseCode == ResponseCode.SEARCHDRIVER ){
				
				JSONObject resultJson = new JSONObject(result.toString());
				mIntent = new Intent(getApplicationContext(),DriverDetailActivity.class);
				mIntent.putExtra("Json", resultJson.toString());
				startActivity(mIntent);
				finish();
				
			}else{
				((MainControllerApplication) getApplication()).showError(getResources().getString(R.string.error_no_data), R.drawable.repeating_bg_error);
			}
		} catch (Exception e) {
			((MainControllerApplication) getApplication()).showError(getResources().getString(R.string.error_service), R.drawable.repeating_bg_error);
		}
	}
	@Override
	public void onServiceError() {
		// TODO Auto-generated method stub
		
	}

}
