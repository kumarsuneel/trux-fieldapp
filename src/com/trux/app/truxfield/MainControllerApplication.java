package com.trux.app.truxfield;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.toolbox.ImageLoader;
import com.android.volley.toolbox.Volley;
import com.trux.app.truxfield.customview.ErrorView;
import com.trux.truxagent.utils.LruBitmapCache;

import android.app.Application;
import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.text.TextUtils;

public class MainControllerApplication extends Application {

	public static final String TAG = MainControllerApplication.class.getSimpleName();
	private ErrorView mErrorView;
	private static MainControllerApplication mInstance;
	private int mRunPoints;
	private RequestQueue mRequestQueue;
	private ImageLoader mImageLoader;

	@Override
	public void onCreate() {
		super.onCreate();

		mInstance = this;
	}

	public static synchronized MainControllerApplication getInstance() {
		return mInstance;
	}

	public RequestQueue getRequestQueue() {
		if (mRequestQueue == null) {
			mRequestQueue = Volley.newRequestQueue(getApplicationContext());
		}

		return mRequestQueue;
	}

	public ImageLoader getImageLoader() {
		getRequestQueue();
		if (mImageLoader == null) {
			mImageLoader = new ImageLoader(this.mRequestQueue, new LruBitmapCache());
		}
		return this.mImageLoader;
	}

	public <T> void addToRequestQueue(Request<T> req, String tag) {
		// set the default tag if tag is empty
		req.setTag(TextUtils.isEmpty(tag) ? TAG : tag);
		getRequestQueue().add(req);
	}

	public <T> void addToRequestQueue(Request<T> req) {
		req.setTag(TAG);
		getRequestQueue().add(req);
	}

	public void cancelPendingRequests(Object tag) {
		if (mRequestQueue != null) {
			mRequestQueue.cancelAll(tag);
		}
	}

	public boolean hasConnection() {
		ConnectivityManager cm = (ConnectivityManager) getBaseContext().getSystemService(Context.CONNECTIVITY_SERVICE);

		@SuppressWarnings("deprecation")
		NetworkInfo wifiNetwork = cm.getNetworkInfo(ConnectivityManager.TYPE_WIFI);
		if (wifiNetwork != null && wifiNetwork.isConnected()) {
			return true;
		}

		@SuppressWarnings("deprecation")
		NetworkInfo mobileNetwork = cm.getNetworkInfo(ConnectivityManager.TYPE_MOBILE);
		if (mobileNetwork != null && mobileNetwork.isConnected()) {
			return true;
		}

		NetworkInfo activeNetwork = cm.getActiveNetworkInfo();
		if (activeNetwork != null && activeNetwork.isConnected()) {
			return true;
		}
		return false;
	}

	public void showError(String errorText, int backgroundResource) {
		if (mErrorView == null) {
			mErrorView = new ErrorView(getApplicationContext());
		}
		mErrorView.show(errorText, backgroundResource);
	}

	public void showError(int errorText, int backgroundResource) {
		if (mErrorView == null) {
			mErrorView = new ErrorView(getApplicationContext());
		}
		mErrorView.show(errorText, backgroundResource);
	}

	public int getRunPoints() {
		return mRunPoints;
	}

	public void setRunPoints(int mRunPoints) {
		this.mRunPoints = mRunPoints;
	}

}
