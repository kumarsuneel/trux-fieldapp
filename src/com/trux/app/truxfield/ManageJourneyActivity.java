package com.trux.app.truxfield;

import java.util.ArrayList;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import com.trux.app.truxfield.model.Driver;
import com.trux.truxagent.manager.AdapterManageJourney;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ListView;

public class ManageJourneyActivity extends AppCompatActivity {
	private ListView manageJourneyLV;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_manage_journey);
		manageJourneyLV = (ListView) findViewById(R.id.manageJourneyLV);
		parseManageJourney(getIntent().getExtras().getString("journey_list"));
		manageJourneyLV.setOnItemClickListener(new OnItemClickListener() {

			@Override
			public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
				Driver driver = (Driver) parent.getItemAtPosition(position);
				switchToDriverTripDetailActivity(driver);
			}

		});
	}

	private void switchToDriverTripDetailActivity(Driver driver) {
		Intent intent = new Intent(this, DriverTripDetailActivity.class);

		intent.putExtra("driver_detail", driver);
		intent.putExtra("from_activity", 1);
		startActivity(intent);
	}

	private void parseManageJourney(String string) {
		JSONObject JsonValue;
		try {
			JsonValue = new JSONObject(string);
			ArrayList<Driver> journeyList = new ArrayList<Driver>();
			JSONArray jsonArray = new JSONArray(JsonValue.getString("data"));
			for (int i = 0; i < jsonArray.length(); i++) {
				JSONObject jsonObject = jsonArray.getJSONObject(i);
				Driver driver = new Driver();
				driver.setLoginStatus(jsonObject.getString("loginstatus"));
				driver.setVehicleType(jsonObject.getString("vehicleType"));
				driver.setVehicleNumber(jsonObject.getString("vehicle_number"));
				driver.setDriverStatus(jsonObject.getString("driverstatus"));
				driver.setDriverName(jsonObject.getString("driverName"));
				driver.setBookingLeaseId(jsonObject.getString("bookingLeaseId"));
				driver.setDriverId(jsonObject.getString("driverId"));
				driver.setDriverPhoneNumber(jsonObject.getString("driver_phone_no"));
				driver.setDriverImage(jsonObject.getString("driverPhoto"));
				journeyList.add(driver);
			}
			AdapterManageJourney adapterManageJourney = new AdapterManageJourney(this, 0, journeyList);
			manageJourneyLV.setAdapter(adapterManageJourney);
		} catch (JSONException e) {

			e.printStackTrace();
		}

	}

}
