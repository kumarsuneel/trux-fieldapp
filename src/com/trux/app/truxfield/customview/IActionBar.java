package com.trux.app.truxfield.customview;

import com.trux.app.truxfield.R;

import android.app.ActionBar;
import android.app.Activity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.ImageButton;
import android.widget.TextView;
import android.widget.Toast;

public class IActionBar {

	public static Activity mActivity;
	public IActionBar(Activity activity) {
		// TODO Auto-generated constructor stub
    	mActivity = activity;
	}
	public void showActionBar(ActionBar mActionBar) {
		
		mActionBar.setDisplayShowHomeEnabled(false);
		mActionBar.setDisplayShowTitleEnabled(false);
		LayoutInflater mInflater = LayoutInflater.from(mActivity);
		View mCustomView = mInflater.inflate(R.layout.header, null);
		TextView mTitleTextView = (TextView) mCustomView.findViewById(R.id.title_text);
		mTitleTextView.setText("My Own Title");
		ImageButton imageButton = (ImageButton) mCustomView.findViewById(R.id.imageButton);
		imageButton.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View view) {
				Toast.makeText(mActivity.getApplicationContext(), "Refresh Clicked!",
						Toast.LENGTH_LONG).show();
			}
		});

		mActionBar.setCustomView(mCustomView);
		mActionBar.setDisplayShowCustomEnabled(true);
	}
}
